﻿using System;

namespace NetEXT
{
    [Serializable]
    public abstract class NotifyChangedBase
    {
        [field: NonSerialized]
        public event Action NotifyChanged;
        protected void OnNotifyChanged()
        {
            if (NotifyChanged != null) NotifyChanged();
        }
    }
}
