﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;

namespace NetEXT.Particles
{
    public class ScaleAffector : AffectorBase
    {
        #region Variables
        private Vector2f _scalefactor = new Vector2f(0, 0);
        #endregion

        #region Properties
        public Vector2f ScaleFactor
        {
            get
            {
                return _scalefactor;
            }
            set
            {
                _scalefactor = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public ScaleAffector(Vector2f NewScaleFactor)
        {
            _scalefactor = NewScaleFactor;
        }
        #endregion

        #region Functions
        public override void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.Scale = new Vector2f(CurrentParticle.Scale.X + (float)(ElapsedTime.Seconds * _scalefactor.X), CurrentParticle.Scale.Y + (float)(ElapsedTime.Seconds * _scalefactor.Y));
        }
        #endregion
    }
}
