﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Particles
{
    public class Particle
    {
        #region Variables
        private Vector2f _position = new Vector2f(0, 0);
        private Vector2f _velocity = new Vector2f(0, 0);
        private float _rotation = 0;
        private float _rotationspeed = 0;
        private Vector2f _scale = new Vector2f(1, 1);
        private Color _color = Color.White;
        private Time _elapsedlifetime = Time.Zero;
        private Time _totallifetime = Time.Zero;
        #endregion

        #region Properties
        public Vector2f Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        public Vector2f Velocity
        {
            get
            {
                return _velocity;
            }
            set
            {
                _velocity = value;
            }
        }
        public float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _rotation = value;
            }
        }
        public float RotationSpeed
        {
            get
            {
                return _rotationspeed;
            }
            set
            {
                _rotationspeed = value;
            }
        }
        public Vector2f Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _scale = value;
            }
        }
        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }
        public Time ElapsedLifetime
        {
            get
            {
                return _elapsedlifetime;
            }
            set
            {
                _elapsedlifetime = value;
            }
        }
        public Time TotalLifetime
        {
            get
            {
                return _totallifetime;
            }
        }
        public Time RemainingLifetime
        {
            get
            {
                return _totallifetime - _elapsedlifetime;
            }
        }
        public float ElapsedLifetimeRatio
        {
            get
            {
                return (float)(ElapsedLifetime.Seconds / TotalLifetime.Seconds);
            }
        }
        public float RemainingLifetimeRatio
        {
            get
            {
                return (float)(RemainingLifetime.Seconds / TotalLifetime.Seconds);
            }
        }
        #endregion

        #region Constructors/Destructors
        public Particle(Time NewTotalLifetime)
        {
            _totallifetime = NewTotalLifetime;
        }
        #endregion
    }
}