﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.Animation;

namespace NetEXT.Particles
{
    public class AnimationAffector : AffectorBase
    {
        #region Variables
        private AnimationBase<Particle> _animation = null;
        #endregion

        #region Properties
        public AnimationBase<Particle> Animation
        {
            get
            {
                return _animation;
            }
            set
            {
                _animation = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public AnimationAffector(AnimationBase<Particle> NewAnimation)
        {
            _animation = NewAnimation;
        }
        #endregion

        #region Functions
        public override void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            _animation.Animate(CurrentParticle, CurrentParticle.ElapsedLifetimeRatio);
        }
        #endregion
    }
}
