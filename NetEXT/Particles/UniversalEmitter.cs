﻿using System;
using NetEXT.TimeFunctions;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Particles
{
    public class UniversalEmitter : EmitterBase
    {
        #region Variables
        private float _emissionrate = 1;
        private float _emissiondifference = 0;
        private Distribution<Vector2f> _particleposition = new Vector2f(0, 0);
        private Distribution<Vector2f> _particlevelocity = new Vector2f(0, 0);
        private Distribution<float> _particlerotation = 0;
        private Distribution<float> _particlerotationspeed = 0;
        private Distribution<Vector2f> _particlescale = new Vector2f(1, 1);
        private Distribution<Color> _particlecolor = Color.White;
        private Distribution<Time> _particlelifetime = Time.Zero;
        #endregion

        #region Properties
        public float EmissionRate
        {
            get
            {
                return _emissionrate;
            }
            set
            {
                _emissionrate = value;
            }
        }
        public Distribution<Vector2f> ParticlePosition
        {
            set
            {
                _particleposition = value;
            }
        }
        public Distribution<Vector2f> ParticleVelocity
        {
            set
            {
                _particlevelocity = value;
            }
        }
        public Distribution<float> ParticleRotation
        {
            set
            {
                _particlerotation = value;
            }
        }
        public Distribution<float> ParticleRotationSpeed
        {
            set
            {
                _particlerotationspeed = value;
            }
        }
        public Distribution<Vector2f> ParticleScale
        {
            set
            {
                _particlescale = value;
            }
        }
        public Distribution<Color> ParticleColor
        {
            set
            {
                _particlecolor = value;
            }
        }
        public Distribution<Time> ParticleLifetime
        {
            set
            {
                _particlelifetime = value;
            }
        }
        #endregion

        #region Functions
        public override void EmitParticles(ParticleSystem CurrentParticleSystem, Time ElapsedTime)
        {
            int numberofparticles = CalculateParticleCount(ElapsedTime);
            for (int i = 1; i <= numberofparticles; i++)
            {
                Particle newparticle = new Particle(_particlelifetime) { Position = _particleposition, Velocity = _particlevelocity, Rotation = _particlerotation, RotationSpeed = _particlerotationspeed, Scale = _particlescale, Color = _particlecolor };
                base.AddParticle(CurrentParticleSystem, newparticle);
            }
        }
        private int CalculateParticleCount(Time ElapsedTime)
        {
            float particleamount = (float)(_emissionrate * ElapsedTime.Seconds) + _emissiondifference;
            int numberofparticles = (int)Math.Floor(particleamount);
            _emissiondifference = particleamount - (float)numberofparticles;
            return numberofparticles;
        }
        #endregion
    }
}
