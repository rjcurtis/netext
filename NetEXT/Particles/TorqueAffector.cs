﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;

namespace NetEXT.Particles
{
    public class TorqueAffector : AffectorBase
    {
        #region Variables
        private float _rotation = 0;
        #endregion

        #region Properties
        private float Rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _rotation = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public TorqueAffector(float NewRotation)
        {
            _rotation = NewRotation;
        }
        #endregion

        #region Functions
        public override void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.RotationSpeed += (float)(ElapsedTime.Seconds * _rotation);
        }
        #endregion
    }
}
