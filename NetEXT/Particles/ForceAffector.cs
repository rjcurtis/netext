﻿using System;
using NetEXT.TimeFunctions;
using SFML.Window;

namespace NetEXT.Particles
{
    public class ForceAffector : AffectorBase
    {
        #region Variables
        private Vector2f _acceleration = new Vector2f(0, 0);
        #endregion

        #region Properties
        public Vector2f Acceleration
        {
            get
            {
                return _acceleration;
            }
            set
            {
                _acceleration = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public ForceAffector(Vector2f NewAcceleration)
        {
            _acceleration = NewAcceleration;
        }
        #endregion

        #region Functions
        public override void ApplyAffector(Particle CurrentParticle, Time ElapsedTime)
        {
            CurrentParticle.Velocity = new Vector2f(CurrentParticle.Velocity.X + (float)(ElapsedTime.Seconds * _acceleration.X), CurrentParticle.Velocity.Y + (float)(ElapsedTime.Seconds * _acceleration.Y));
        }
        #endregion
    }
}
