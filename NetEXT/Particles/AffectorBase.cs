﻿using System;
using NetEXT.TimeFunctions;

namespace NetEXT.Particles
{
    public abstract class AffectorBase
    {
        public abstract void ApplyAffector(Particle CurrentParticle, Time ElapsedTime);
    }
}
