﻿using System;
using System.Collections.Generic;
using NetEXT.TimeFunctions;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Particles
{
    public class ParticleSystem : Transformable, Drawable
    {
        #region Variables
        private Texture _texture = null;
        private IntRect _texturerect = new IntRect(0, 0, 1, 1);
        private List<TimeLink<ExpiringTime, EmitterBase>> _emitters = new List<TimeLink<ExpiringTime, EmitterBase>>();
        private List<TimeLink<ExpiringTime, AffectorBase>> _affectors = new List<TimeLink<ExpiringTime, AffectorBase>>();
        private List<Particle> _particles = new List<Particle>();
        private VertexArray _vertexarray = new VertexArray(PrimitiveType.Quads);
        private bool _needsvertexupdate = true;
        #endregion

        #region Properties
        public Texture Texture
        {
            get
            {
                return _texture;
            }
            set
            {
                _texture = value;
                _texturerect = new IntRect(0, 0, (int)value.Size.X, (int)value.Size.Y);
                _needsvertexupdate = true;
            }
        }
        public IntRect TextureRect
        {
            get
            {
                return _texturerect;
            }
            set
            {
                _texturerect = value;
                _needsvertexupdate = true;
            }
        }
        public EmitterBase[] Emitters
        {
            get
            {
                EmitterBase[] emitters = new EmitterBase[_emitters.Count];
                for (int i = 0; i < _emitters.Count; i++) { emitters[i] = _emitters[i].Value; }
                return emitters;
            }
        }
        public AffectorBase[] Affectors
        {
            get
            {
                AffectorBase[] affectors = new AffectorBase[_affectors.Count];
                for (int i = 0; i < _affectors.Count; i++) { affectors[i] = _affectors[i].Value; }
                return affectors;
            }
        }
        public Particle[] Particles
        {
            get
            {
                return _particles.ToArray();
            }
        }
        #endregion

        #region Constructors/Destructors
        public ParticleSystem(Texture NewTexture)
        {
            _texture = NewTexture;
            _texturerect = new IntRect(0, 0, (int)_texture.Size.X, (int)_texture.Size.Y);
        }
        public ParticleSystem(Texture NewTexture, IntRect NewTextureRect)
        {
            _texture = NewTexture;
            _texturerect = NewTextureRect;
        }
        public ParticleSystem(IntRect ParticleSize)
        {
            _texture = null;
            _texturerect = ParticleSize;
        }
        #endregion

        #region Functions
        public void Draw(RenderTarget target, RenderStates states)
        {
            UpdateVertices();
            states.Texture = null;
            if (_texture != null) states.Texture = _texture;
            states.Transform = base.Transform;
            target.Draw(_vertexarray, states);
        }
        private void UpdateVertices()
        {
            if (!_needsvertexupdate) return;
            _needsvertexupdate = false;
            _vertexarray.Clear();
            Vector2f topleftoffset = new Vector2f(-(_texturerect.Width / 2), -(_texturerect.Height / 2));
            Vector2f toprightoffset = new Vector2f((_texturerect.Width / 2), -(_texturerect.Height / 2));
            Vector2f bottomrightoffset = new Vector2f((_texturerect.Width / 2), (_texturerect.Height / 2));
            Vector2f bottomleftoffset = new Vector2f(-(_texturerect.Width / 2), (_texturerect.Height / 2));
            foreach (Particle CurrentParticle in _particles)
            {
                Transform transform = Transform.Identity;
                transform.Translate(CurrentParticle.Position);
                transform.Rotate(CurrentParticle.Rotation);
                transform.Scale(CurrentParticle.Scale);
                _vertexarray.Append(new Vertex(transform.TransformPoint(topleftoffset), CurrentParticle.Color, new Vector2f((float)_texturerect.Left, (float)_texturerect.Top)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(toprightoffset), CurrentParticle.Color, new Vector2f((float)_texturerect.Left + (float)_texturerect.Width, (float)_texturerect.Top)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(bottomrightoffset), CurrentParticle.Color, new Vector2f((float)_texturerect.Left + (float)_texturerect.Width, (float)_texturerect.Top + (float)_texturerect.Height)));
                _vertexarray.Append(new Vertex(transform.TransformPoint(bottomleftoffset), CurrentParticle.Color, new Vector2f((float)_texturerect.Left, (float)_texturerect.Top + (float)_texturerect.Height)));

            }
        }
        public void Update(Time DeltaTime)
        {
            _needsvertexupdate = true;
            for (int i = 0; i < _emitters.Count; )
            {
                _emitters[i].Value.EmitParticles(this, DeltaTime);
                if (_emitters[i].Time.TimeUntilExpire > Time.Zero)
                {
                    _emitters[i].Time.ElapsedTime += DeltaTime;
                    if (_emitters[i].Time.ElapsedTime >= _emitters[i].Time.TimeUntilExpire)
                    {
                        _emitters.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    i++;
                }
            }
            List<Particle> eraselist = null;
            foreach (Particle particle in _particles)
            {
                particle.ElapsedLifetime += DeltaTime;
                if (particle.ElapsedLifetime > particle.TotalLifetime)
                {
                    if (eraselist == null) eraselist = new List<Particle>();
                    eraselist.Add(particle);
                }
                else
                {
                    particle.Position = new Vector2f(particle.Position.X + (float)(DeltaTime.Seconds * particle.Velocity.X), particle.Position.Y + (float)(DeltaTime.Seconds * particle.Velocity.Y));
                    particle.Rotation += (float)(DeltaTime.Seconds * particle.RotationSpeed);
                    foreach (TimeLink<ExpiringTime, AffectorBase> affector in _affectors)
                    {
                        affector.Value.ApplyAffector(particle, DeltaTime);
                    }
                }
            }
            if (eraselist != null)
            {
                foreach (Particle CurrentParticle in eraselist)
                {
                    _particles.Remove(CurrentParticle);
                }
                eraselist = null;
            }
            for (int i = 0; i < _affectors.Count; )
            {
                if (_affectors[i].Time.TimeUntilExpire > Time.Zero)
                {
                    _affectors[i].Time.ElapsedTime += DeltaTime;
                    if (_affectors[i].Time.ElapsedTime >= _affectors[i].Time.TimeUntilExpire)
                    {
                        _affectors.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    i++;
                }
            }
        }
        public void AddEmitter(EmitterBase NewEmitter)
        {
            AddEmitter(NewEmitter, Time.Zero);
        }
        public void AddEmitter(EmitterBase NewEmitter, Time TimeToLive)
        {
            TimeLink<ExpiringTime, EmitterBase> emitter = new TimeLink<ExpiringTime, EmitterBase>(new ExpiringTime(TimeToLive), NewEmitter);
            if (!_emitters.Contains(emitter)) _emitters.Add(emitter);
        }
        public void AddAffector(AffectorBase NewAffector)
        {
            AddAffector(NewAffector, Time.Zero);
        }
        public void AddAffector(AffectorBase NewAffector, Time TimeToLive)
        {
            TimeLink<ExpiringTime, AffectorBase> affector = new TimeLink<ExpiringTime, AffectorBase>(new ExpiringTime(TimeToLive), NewAffector);
            if (!_affectors.Contains(affector)) _affectors.Add(affector);
        }
        internal void AddParticle(Particle NewParticle)
        {
            _particles.Add(NewParticle);
            _needsvertexupdate = true;
        }
        public void RemoveEmitter(EmitterBase CurrentEmitter)
        {
            for (int i = 0; i < _emitters.Count; )
            {
                if (_emitters[i].Value == CurrentEmitter)
                {
                    _emitters.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        public void RemoveAffector(AffectorBase CurrentAffector)
        {
            for (int i = 0; i < _affectors.Count; )
            {
                if (_affectors[i].Value == CurrentAffector)
                {
                    _affectors.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        public void RemoveParticle(Particle CurrentParticle)
        {
            if (_particles.Contains(CurrentParticle))
            {
                _particles.Remove(CurrentParticle);
                _needsvertexupdate = true;
            }
        }
        public void ClearEmitters()
        {
            _emitters.Clear();
        }
        public void ClearAffectors()
        {
            _affectors.Clear();
        }
        public void ClearParticles()
        {
            _particles.Clear();
            _needsvertexupdate = true;
        }
        #endregion

        #region Additional Definitions
        private class ExpiringTime
        {
            #region Variables
            private Time _elapsedtime = Time.Zero;
            private Time _timeuntilexpire = -Time.Zero;
            #endregion

            #region Properties
            public Time ElapsedTime
            {
                get
                {
                    return _elapsedtime;
                }
                set
                {
                    _elapsedtime = value;
                }
            }
            public Time TimeUntilExpire
            {
                get
                {
                    return _timeuntilexpire;
                }
                set
                {
                    _timeuntilexpire = value;
                }
            }
            #endregion

            #region Constructors/Destructors
            public ExpiringTime(Time NewTimeUntilExpire)
            {
                _timeuntilexpire = NewTimeUntilExpire;
            }
            #endregion

            #region Functions
            public override bool Equals(object obj)
            {
                if (((ExpiringTime)obj).TimeUntilExpire == TimeUntilExpire) return true;
                else return false;
            }
            #endregion
        }
        private class TimeLink<TimeType, ValueType>
        {
            #region Variables
            private TimeType _time;
            private ValueType _value;
            #endregion

            #region Properties
            public TimeType Time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                }
            }
            public ValueType Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    _value = value;
                }
            }
            #endregion

            #region Constructors/Destructors
            public TimeLink(TimeType NewTime, ValueType NewValue)
            {
                _time = NewTime;
                _value = NewValue;
            }
            #endregion

            #region Functions
            public override bool Equals(object obj)
            {
                if (((TimeLink<TimeType, ValueType>)obj).Value.Equals(_value)) return true;
                else return false;
            }
            #endregion
        }
        #endregion
    }
}
