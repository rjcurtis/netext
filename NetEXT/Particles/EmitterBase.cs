﻿using System;
using NetEXT.TimeFunctions;

namespace NetEXT.Particles
{
    public abstract class EmitterBase
    {
        public abstract void EmitParticles(ParticleSystem CurrentParticleSystem, Time ElapsedTime);
        protected void AddParticle(ParticleSystem CurrentParticleSystem, Particle NewParticle)
        {
            CurrentParticleSystem.AddParticle(NewParticle);
        }
    }
}
