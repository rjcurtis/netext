﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetEXT.MathFunctions
{
    public class Distribution<ValueType>
    {
        #region Delegates
        public delegate ValueType ValueDelegate();
        #endregion

        #region Variables
        private ValueDelegate _callback = null;
        private ValueType _value;
        #endregion

        #region Constructors/Destructors
        public Distribution(ValueType Value)
        {
            _value = Value;
        }
        public Distribution(ValueDelegate Callback)
        {
            _callback = Callback;
        }
        #endregion

        #region Operators
        public static implicit operator Distribution<ValueType>(ValueType Value)
        {
            return new Distribution<ValueType>(Value);
        }
        public static implicit operator Distribution<ValueType>(ValueDelegate Callback)
        {
            return new Distribution<ValueType>(Callback);
        }
        public static implicit operator ValueType(Distribution<ValueType> Distribution)
        {
            if (Distribution._callback != null) return Distribution._callback();
            else return Distribution._value;
        }
        #endregion
    }
}
