﻿using System;
using SFML.Window;
using NetEXT.TimeFunctions;
using NetEXT.Vectors;

namespace NetEXT.MathFunctions
{
    public static class Distributions
    {
        public static Distribution<float> Uniform(float Begin, float End)
        {
            return new Distribution<float>.ValueDelegate(() =>
            {
                return RandomGenerator.Random(Begin, End);
            });
        }
        public static Distribution<Time> Uniform(Time Begin, Time End)
        {
            return new Distribution<Time>.ValueDelegate(() =>
            {
                return Time.FromSeconds(RandomGenerator.Random((float)Begin.Seconds, (float)End.Seconds));
            });
        }
        public static Distribution<Vector2f> Rect(Vector2f Center, Vector2f HalfSize)
        {
            return new Distribution<Vector2f>.ValueDelegate(() =>
            {
                return new Vector2f(RandomGenerator.RandomDev(Center.X, HalfSize.X), RandomGenerator.RandomDev(Center.Y, HalfSize.Y));
            });
        }
        public static Distribution<Vector2f> Circle(Vector2f Center, float Radius)
        {
            return new Distribution<Vector2f>.ValueDelegate(() =>
            {
                Vector2f radiusvector = new PolarVector(Radius * (float)Trigonometry.SqrRoot(RandomGenerator.Random(0f, 1f)), RandomGenerator.Random(0f, 360f));
                return Center + radiusvector;
            });
        }
        public static Distribution<Vector2f> Deflect(Vector2f Direction, float MaxRotation)
        {
            return new Distribution<Vector2f>.ValueDelegate(() =>
            {
                return Vectors.Vector2Algebra.Rotate(Direction, RandomGenerator.RandomDev(0f, MaxRotation));
            });
        }
    }
}
