﻿using System;

namespace NetEXT.MathFunctions
{
    public static class Trigonometry
    {
        #region Functions
        public static double DegtoRad(double Deg)
        {
            return PI / 180d * Deg;
        }
        public static double RadtoDeg(double Rad)
        {
            return 180d / PI * Rad;
        }
        public static double Sin(double Deg)
        {
            return Math.Sin(DegtoRad(Deg));
        }
        public static double Cos(double Deg)
        {
            return Math.Cos(DegtoRad(Deg));
        }
        public static double Tan(double Deg)
        {
            return Math.Tan(DegtoRad(Deg));
        }
        public static double ArcSin(double Sin)
        {
            return RadtoDeg(Math.Asin(Sin));
        }
        public static double ArcCos(double Cos)
        {
            return RadtoDeg(Math.Acos(Cos));
        }
        public static double ArcTan(double Tan)
        {
            return RadtoDeg(Math.Atan(Tan));
        }
        public static double ArcTan2(double Y, double X)
        {
            return RadtoDeg(Math.Atan2(Y, X));
        }
        public static double SqrRoot(double Value)
        {
            return Math.Sqrt(Value);
        }
        public const double PI = Math.PI;
        #endregion
    }
}
