﻿using System;

namespace NetEXT.MathFunctions
{
    public static class RandomGenerator
    {
        #region Variables
        private static Random _random = new Random(Guid.NewGuid().GetHashCode());
        #endregion

        #region Functions
        public static void SetRandomSeed(int Seed)
        {
            _random = new Random(Seed);
        }
        public static int Random(int Begin, int End)
        {
            return _random.Next(Begin, End);
        }
        public static int RandomDev(int Middle, int Deviation)
        {
            return Random(Middle - Deviation, Middle + Deviation);
        }
        public static float Random(float Begin, float End)
        {
            return (float)_random.Next((int)(Begin * 1000f), (int)(End * 1000f)) / 1000f;
        }
        public static float RandomDev(float Middle, float Deviation)
        {
            return Random(Middle - Deviation, Middle + Deviation);
        }
        #endregion
    }
}
