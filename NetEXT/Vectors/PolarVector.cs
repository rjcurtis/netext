﻿using System;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Vectors
{
    public struct PolarVector
    {
        #region Variables
        public float R;
        public float A;
        #endregion

        #region Constructors/Destructors
        public PolarVector(float NewR, float NewA)
        {
            R = NewR;
            A = NewA;
        }
        public PolarVector(Vector2i Vector) : this(new Vector2f(Vector.X, Vector.Y)) { }
        public PolarVector(Vector2f Vector)
        {
            R = Vector2Algebra.Length(Vector);
            if (Vector.X == 0 && Vector.Y == 0) A = 0;
            else A = Vector2Algebra.PolarAngle(Vector);
        }
        #endregion

        #region Operators
        public static implicit operator Vector2f(PolarVector Vector)
        {
            return new Vector2f(Vector.R * (float)Trigonometry.Cos(Vector.A), Vector.R * (float)Trigonometry.Sin(Vector.A));
        }
        public static implicit operator Vector2i(PolarVector Vector)
        {
            return new Vector2i((int)Math.Round(Vector.R * (float)Trigonometry.Cos(Vector.A), 0), (int)Math.Round(Vector.R * (float)Trigonometry.Sin(Vector.A), 0));
        }
        #endregion
    }
}
