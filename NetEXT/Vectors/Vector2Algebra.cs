﻿using System;
using NetEXT.MathFunctions;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Vectors
{
    public static class Vector2Algebra
    {
        #region Functions
        public static float Length(Vector2f Vector)
        {
            return (float)Trigonometry.SqrRoot(SquaredLength(Vector));
        }
        public static Vector2f Length(Vector2f Vector, float NewLength)
        {
            float length = Length(Vector);
            return new Vector2f(Vector.X * NewLength / length, Vector.Y * NewLength / length);
        }
        public static float SquaredLength(Vector2f Vector)
        {
            return DotProduct(Vector, Vector);
        }
        public static Vector2f UnitVector(Vector2f Vector)
        {
            float len = Length(Vector);
            return new Vector2f(Vector.X / len, Vector.Y / len);
        }
        public static float PolarAngle(Vector2f Vector)
        {
            return (float)Trigonometry.ArcTan2(Vector.X, Vector.Y);
        }
        public static Vector2f PolarAngle(Vector2f Vector, float Angle)
        {
            float length = Length(Vector);
            return new Vector2f(length * (float)Trigonometry.Cos(Angle), length * (float)Trigonometry.Sin(Angle));
        }
        public static Vector2f Rotate(Vector2f Vector, float Angle)
        {
            float cos = (float)Trigonometry.Cos(Angle);
            float sin = (float)Trigonometry.Sin(Angle);
            return new Vector2f((cos * Vector.X) - (sin * Vector.Y), (sin * Vector.X) + (cos * Vector.Y));
        }
        public static Vector2f PerpendicularVector(Vector2f Vector)
        {
            return new Vector2f(-Vector.Y, Vector.X);
        }
        public static float SignedAngle(Vector2f LHS, Vector2f RHS)
        {
            return (float)Trigonometry.ArcTan2(CrossProduct(LHS, RHS), DotProduct(LHS, RHS));
        }
        public static float DotProduct(Vector2f LHS, Vector2f RHS)
        {
            return (LHS.X * RHS.X) + (LHS.Y * RHS.Y);
        }
        public static float CrossProduct(Vector2f LHS, Vector2f RHS)
        {
            return (LHS.X * RHS.Y) - (LHS.Y * RHS.X);
        }
        public static Vector2f CWiseProduct(Vector2f LHS, Vector2f RHS)
        {
            return new Vector2f(LHS.X * RHS.X, LHS.Y * RHS.Y);
        }
        public static Vector2f CWiseQuotient(Vector2f LHS, Vector2f RHS)
        {
            return new Vector2f(LHS.X / RHS.X, LHS.Y / RHS.Y);
        }
        #endregion
    }
}
