﻿namespace NetEXT.Networking
{
    public enum SynchronizationType
    {
        Static = 0,
        Dynamic = 1,
        Stream = 2
    }
}
