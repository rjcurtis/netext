﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NetEXT.Networking
{
    public abstract class SynchronizerBase
    {
        #region Consts
        protected const int StreamID = 200;
        #endregion

        #region Enums
        protected enum SyncType : int
        {
            Create = 100,
            Update = 101,
            Destroy = 102
        }
        #endregion

        #region Variables
        protected List<Link> _links = new List<Link>();
        protected List<SyncedObject> _objects = new List<SyncedObject>();
        #endregion

        #region Properties
        public Link[] ActiveLinks
        {
            get
            {
                return _links.ToArray();
            }
        }
        #endregion

        #region Functions
        internal abstract void AddObject(SyncedObject Object);
        internal abstract void RemoveObject(SyncedObject Object);
        internal abstract void UpdateObject(SyncedObject Object);
        #endregion
    }
    public class SynchronizerServer : SynchronizerBase
    {
        #region Variables
        private Dictionary<SyncedObject, SyncType> _updates = new Dictionary<SyncedObject, SyncType>();
        #endregion

        #region Functions
        public void AddClient(Link ClientLink)
        {
            if (_links.Contains(ClientLink)) return;
            _links.Add(ClientLink);
            foreach (var syncobj in _objects)
            {
                Message message = syncobj.Serialize(SynchronizationType.Static);
                message.Position = 0;
                message.WriteInt((int)SyncType.Create);
                message.WriteLong(syncobj.ID);
                message.WriteLong(syncobj.TypeID);
                ClientLink.SendMessage(StreamID, message);
            }
        }
        public void RemoveClient(Link ClientLink)
        {
            if (!_links.Contains(ClientLink)) return;
            _links.Remove(ClientLink);
        }
        public void Update()
        {
            foreach (var syncobj in _objects)
            {
                syncobj.CheckStreamUpdate();
            }
            Send();
        }
        private void Send()
        {
            foreach (var update in _updates)
            {
                Message message = null;
                if (update.Value == SyncType.Create)
                {
                    message = update.Key.Serialize(SynchronizationType.Static);
                    message.Position = 0;
                    message.WriteInt((int)SyncType.Create);
                    message.WriteLong(update.Key.ID);
                    message.WriteLong(update.Key.TypeID);
                }
                else if (update.Value == SyncType.Update)
                {
                    message = update.Key.Serialize(SynchronizationType.Dynamic);
                    message.Position = 0;
                    message.WriteInt((int)SyncType.Update);
                    message.WriteLong(update.Key.ID);
                }
                else if (update.Value == SyncType.Destroy)
                {
                    message = new Message();
                    message.WriteInt((int)SyncType.Destroy);
                    message.WriteLong(update.Key.ID);
                }
                List<Link> removelist = null;
                foreach (Link clientlink in _links)
                {
                    if (clientlink.IsConnected)
                    {
                        clientlink.SendMessage(StreamID, message);
                    }
                    else
                    {
                        if (removelist == null) removelist = new List<Link>();
                        removelist.Add(clientlink);
                    }
                }
                if (removelist != null)
                {
                    foreach (Link clientlink in removelist)
                    {
                        _links.Remove(clientlink);
                    }
                }
            }
            _updates.Clear();
        }
        internal override void AddObject(SyncedObject Object)
        {
            if (_objects.Contains(Object) || Object.ID == SyncedObject.InvalidID) return;
            _objects.Add(Object);
            _updates.Add(Object, SyncType.Create);
        }
        internal override void RemoveObject(SyncedObject Object)
        {
            if (!_objects.Contains(Object)) return;
            _objects.Remove(Object);
            if (_updates.ContainsKey(Object))
            {
                if (_updates[Object] == SyncType.Create)
                {
                    _updates.Remove(Object);
                }
                else if (_updates[Object] == SyncType.Update)
                {
                    _updates[Object] = SyncType.Destroy;
                }
            }
            else _updates.Add(Object, SyncType.Destroy);
        }
        internal override void UpdateObject(SyncedObject Object)
        {
            if (!_objects.Contains(Object)) return;
            if (_updates.ContainsKey(Object) && _updates[Object] != SyncType.Create) _updates[Object] = SyncType.Update;
            else if (!_updates.ContainsKey(Object)) _updates.Add(Object, SyncType.Update);
        }
        public T CreateObject<T>() where T : SyncedObject
        {
            T newsyncedobject = Activator.CreateInstance<T>();
            newsyncedobject.SetSynchronizer(this);
            return newsyncedobject;
        }
        public T CreateObject<T>(object[] Parameters) where T : SyncedObject
        {
            T newsyncedobject = (T)Activator.CreateInstance(typeof(T), Parameters);
            newsyncedobject.SetSynchronizer(this);
            return newsyncedobject;
        }
        #endregion
    }
    public class SynchronizerClient : SynchronizerBase
    {
        #region Variables
        private Dictionary<long, Func<SyncedObject>> _constructors = new Dictionary<long, Func<SyncedObject>>();
        private Dictionary<long, Action<SyncedObject>> _destructors = new Dictionary<long, Action<SyncedObject>>();
        #endregion

        #region Functions
        public void AddServer(Link ServerLink)
        {
            if (_links.Contains(ServerLink)) return;
            _links.Add(ServerLink);
        }
        public void RemoveServer(Link ServerLink)
        {
            if (!_links.Contains(ServerLink)) return;
            _links.Remove(ServerLink);
        }
        public void Update()
        {
            Recieve();
        }
        public void SetLifetimeManager(long TypeID, Func<SyncedObject> Constructor, Action<SyncedObject> Destructor)
        {
            if (_constructors.ContainsKey(TypeID) || _destructors.ContainsKey(TypeID)) return;
            _constructors.Add(TypeID, Constructor);
            _destructors.Add(TypeID, Destructor);
        }
        private void Recieve()
        {
            List<Link> removelist = null;
            foreach (Link serverlink in _links)
            {
                if (serverlink.IsConnected)
                {
                    bool finished = false;
                    while (!finished)
                    {
                        Message msg = serverlink.ReadMessage(StreamID);
                        if (msg.Size <= 0) finished = true;
                        else
                        {
                            if (msg.Size >= 12)
                            {
                                SyncType updatetype = (SyncType)msg.ReadInt();
                                long id = msg.ReadLong();
                                if (updatetype == SyncType.Create)
                                {
                                    long typeid = msg.ReadLong();
                                    SyncedObject obj = _constructors[typeid]();
                                    obj.SetSynchronizer(this);
                                    obj.ID = id;
                                    obj.Deserialize(msg);
                                }
                                else if (updatetype == SyncType.Update)
                                {
                                    SyncedObject obj = (from syncobj in _objects where syncobj.ID == id select syncobj).First();
                                    obj.Deserialize(msg);
                                }
                                else if (updatetype == SyncType.Destroy)
                                {
                                    SyncedObject obj = (from syncobj in _objects where syncobj.ID == id select syncobj).First();
                                    _destructors[obj.TypeID](obj);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (removelist == null) removelist = new List<Link>();
                    removelist.Add(serverlink);
                }
            }
            if (removelist != null)
            {
                foreach (Link serverlink in removelist)
                {
                    _links.Remove(serverlink);
                }
            }
        }
        internal override void AddObject(SyncedObject Object)
        {
            if (_objects.Contains(Object) || Object.ID == SyncedObject.InvalidID) return;
            _objects.Add(Object);
        }
        internal override void RemoveObject(SyncedObject Object)
        {
            if (!_objects.Contains(Object)) return;
            _objects.Remove(Object);
        }
        internal override void UpdateObject(SyncedObject Object) { }
        #endregion
    }
}
