﻿using System;
using System.Reflection;
using System.Collections.Generic;
using NetEXT.TimeFunctions;

namespace NetEXT.Networking
{
    public abstract class SyncedObject
    {
        public bool SENTCREATED = false;

        #region Consts
        internal const long InvalidID = long.MinValue;
        private const long InitialID = long.MinValue + 1;
        #endregion

        #region Globals
        private static long IDCounter = InitialID;
        private static Time _synchronizationperiod = Time.FromSeconds(1);
        #endregion

        #region Variables
        private List<SyncedTypeBase> _syncedproperties = new List<SyncedTypeBase>();
        private long _id = InvalidID;
        private Clock _synctime = null;
        private SynchronizerBase _synchronizer = null;
        private bool _disposed = false;
        #endregion

        #region Properties
        internal SyncedTypeBase[] SyncedProperties
        {
            get
            {
                return _syncedproperties.ToArray();
            }
        }
        internal long ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public abstract long TypeID { get; }
        public static Time SynchronizationPeriod
        {
            get
            {
                return _synchronizationperiod;
            }
            set
            {
                _synchronizationperiod = value;
            }
        }
        #endregion

        #region Constructors / Destructors
        public SyncedObject()
        {
            _id = IDCounter;
            IDCounter += 1;
        }
        ~SyncedObject()
        {
            if (!_disposed) Dipose();
        }
        #endregion

        #region Functions
        public void Dipose()
        {
            if (_disposed) return;
            _disposed = true;
            if (_synchronizer != null) _synchronizer.RemoveObject(this);
        }
        internal void RegisterMemeber(SyncedTypeBase SyncedType)
        {
            if (!_disposed)
            {
                _syncedproperties.Add(SyncedType);
                if (_synctime == null && SyncedType.SynchronizationType == SynchronizationType.Stream) _synctime = new Clock();
                SyncedType.NotifyChanged += NotifyChanged;
            }
        }
        internal Message Serialize(SynchronizationType SynchronizationType)
        {
            if (!_disposed)
            {
                Message msg = new Message();
                msg.WriteInt((int)SynchronizationType);
                foreach (SyncedTypeBase property in _syncedproperties)
                {
                    property.Serialize(msg, SynchronizationType);
                }
                return msg;
            }
            else return null;
        }
        internal void Deserialize(Message Message)
        {
            if (!_disposed)
            {
                SynchronizationType type = (SynchronizationType)Message.ReadInt();
                foreach (SyncedTypeBase property in _syncedproperties)
                {
                    property.Deserialize(Message, type);
                }
            }
        }
        private void NotifyChanged()
        {
            if (!_disposed)
            {
                if (_synctime != null && _synchronizer != null)
                {
                    _synctime.Restart();
                    _synchronizer.UpdateObject(this);
                }
            }
        }
        internal void CheckStreamUpdate()
        {
            if (!_disposed)
            {
                if (_synctime != null && _synchronizer != null)
                {
                    if (_synctime.ElapsedTime >= SynchronizationPeriod)
                    {
                        _synctime.Restart();
                        _synchronizer.UpdateObject(this);
                    }
                }
            }
        }
        internal void SetSynchronizer(SynchronizerBase Synchronizer)
        {
            if (!_disposed)
            {
                if (_synchronizer != null) _synchronizer.RemoveObject(this);
                _synchronizer = Synchronizer;
                _synchronizer.AddObject(this);
            }
        }
        #endregion
    }
}
