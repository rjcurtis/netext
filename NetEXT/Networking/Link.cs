﻿using System;
using System.Collections.Generic;

namespace NetEXT.Networking
{
    public class Link
    {
        #region Variables
        private ReliableTransport _transport = null;
        private Dictionary<int, List<byte[]>> _recievedblocks = new Dictionary<int, List<byte[]>>();
        #endregion

        #region Properties
        public bool IsConnected
        {
            get
            {
                return _transport.IsConnected;
            }
        }
        public int SendingBufferLength
        {
            get
            {
                return _transport.SendingBufferLength;
            }
        }
        public int ReceivingBufferLength
        {
            get
            {
                return _transport.ReceivingBufferLength;
            }
        }
        #endregion

        #region Constructors
        public Link(ReliableTransport Transport)
        {
            _transport = Transport;
            _transport.DataRecieved += DataReceived;
        }
        #endregion

        #region Functions
        private void DataReceived(ReliableTransport Transport)
        {
            Transport.Lock();
            bool finished = false;
            int length = Transport.ReceivingBufferLength;
            while (length > 8 && !finished)
            {
                byte[] header = Transport.PeakReceivingBuffer(8);
                int streamid = ToInt(header, 0);
                int size = ToInt(header, 4);
                if (length >= size + 8)
                {
                    byte[] payload = Transport.HandleReceivingBuffer(size + 8);
                    byte[] block = new byte[size];
                    Array.Copy(payload, 8, block, 0, size);
                    if (!_recievedblocks.ContainsKey(streamid)) _recievedblocks.Add(streamid, new List<byte[]>());
                    _recievedblocks[streamid].Add(block);
                }
                else
                {
                    finished = true;
                }
                length = Transport.ReceivingBufferLength;
            }
            Transport.ReleaseLock();
        }
        private static int ToInt(byte[] Data, int Offset)
        {
            byte[] intarray = new byte[4];
            Array.Copy(Data, Offset, intarray, 0, 4);
            if (BitConverter.IsLittleEndian) Array.Reverse(intarray);
            return BitConverter.ToInt32(intarray, 0);
        }
        private static byte[] FromInt(int Value)
        {
            byte[] ret = BitConverter.GetBytes(Value);
            if (BitConverter.IsLittleEndian) Array.Reverse(ret);
            return ret;
        }
        public byte[] ReadBlock(int StreamID)
        {
            if (!_recievedblocks.ContainsKey(StreamID) || _recievedblocks[StreamID].Count <= 0)
            {
                return new byte[0];
            }
            byte[] retval = _recievedblocks[StreamID][0];
            _recievedblocks[StreamID].RemoveAt(0);
            return retval;
        }
        public Message ReadMessage(int StreamID)
        {
            return new Message(ReadBlock(StreamID));
        }
        public void SendBlock(int StreamID, byte[] Block)
        {
            byte[] payload = new byte[Block.Length + 8];
            Array.Copy(FromInt(StreamID), 0, payload, 0, 4);
            Array.Copy(FromInt(Block.Length), 0, payload, 4, 4);
            Array.Copy(Block, 0, payload, 8, Block.Length);
            _transport.Lock();
            _transport.SendData(payload);
            _transport.ReleaseLock();
        }
        public void SendMessage(int StreamID, Message Message)
        {
            SendBlock(StreamID, Message.Buffer);
        }
        public void ClearBuffers()
        {
            _transport.Lock();
            _transport.ClearBuffers();
            _transport.ReleaseLock();
        }
        public void Disconnect()
        {
            _transport.Lock();
            _transport.Disconnect();
            _transport.ReleaseLock();
        }
        public void Connect()
        {
            _transport.Lock();
            _transport.Connect();
            _transport.ReleaseLock();
        }
        public void Shutdown()
        {
            _transport.Lock();
            _transport.Shutdown();
            _transport.ReleaseLock();
        }
        #endregion

        #region Operators
        public static implicit operator Link(ReliableTransport Transport)
        {
            return new Link(Transport);
        }
        #endregion
    }
}
