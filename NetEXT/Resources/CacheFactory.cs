﻿using System;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;

namespace NetEXT.Resources
{
    public static class CacheFactory
    {
        public static ResourceCache<Texture> CreateTextureCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Texture>(ConstructorFactory.GenerateTextureConstructors(), ResourceStrategy);
        }
        public static ResourceCache<Image> CreateImageCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Image>(ConstructorFactory.GenerateImageConstructors(), ResourceStrategy);
        }
        public static ResourceCache<Font> CreateFontCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Font>(ConstructorFactory.GenerateFontConstructors(), ResourceStrategy);
        }
        public static ResourceCache<Shader> CreateShaderCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<Shader>(ConstructorFactory.GenerateShaderConstructors(), ResourceStrategy);
        }
        public static ResourceCache<SoundBuffer> CreateSoundBufferCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new ResourceCache<SoundBuffer>(ConstructorFactory.GenerateSoundBufferConstructors(), ResourceStrategy);
        }
        public static MultiResourceCache CreateMultiResourceCache(ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            return new MultiResourceCache(ConstructorFactory.GenerateTextureConstructors() + ConstructorFactory.GenerateImageConstructors() + ConstructorFactory.GenerateFontConstructors() + ConstructorFactory.GenerateShaderConstructors() + ConstructorFactory.GenerateSoundBufferConstructors(), ResourceStrategy);
        }
    }
}
