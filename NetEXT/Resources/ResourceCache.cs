﻿using System;
using System.Collections.Generic;

namespace NetEXT.Resources
{
    public class ResourceCache<T> where T : IDisposable
    {
        #region Variables
        private ResourceStrategies _resourcestrategy = ResourceStrategies.AutoRelease;
        private Dictionary<object, ResourceHolder<T>> _resources = new Dictionary<object, ResourceHolder<T>>();
        private DynamicDelegate _constructiondelegate = null;
        #endregion

        #region Functions
        public ResourceStrategies ResourceStrategy
        {
            get
            {
                return _resourcestrategy;
            }
            set
            {
                _resourcestrategy = value;
            }
        }
        public DynamicDelegate ConstructionDelegate
        {
            get
            {
                return _constructiondelegate;
            }
            set
            {
                _constructiondelegate = value;
            }
        }
        #endregion

        #region Contructors
        public ResourceCache(DynamicDelegate ConstructionDelegate, ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            _resourcestrategy = ResourceStrategy;
            _constructiondelegate = ConstructionDelegate;
        }
        #endregion

        #region Functions
        public ResourceHandle<T> Aquire(object Key) { return Aquire(Key, new object[0]); }
        public ResourceHandle<T> Aquire(object Key, object Parameter) { return Aquire(Key, new object[] { Parameter }); }
        public ResourceHandle<T> Aquire(object Key, object[] Parameters)
        {
            if (_resources.ContainsKey(Key)) return new ResourceHandle<T>(_resources[Key]);
            else
            {
                ResourceHolder<T> newresource = new ResourceHolder<T>(ConstructionDelegate.InvokeDelegate<T>(Parameters));
                _resources.Add(Key, newresource);
                newresource.ResourceNotReferenced += ResourceNotReferenced;
                newresource.Key = Key;
                return new ResourceHandle<T>(newresource);
            }
        }
        public void Release(object Key)
        {
            if (!_resources.ContainsKey(Key)) return;
            _resources[Key].DisposeResource();
            _resources[Key].ResourceNotReferenced -= ResourceNotReferenced;
            _resources.Remove(Key);
        }
        private void ResourceNotReferenced(ResourceHolder<T> Resource)
        {
            if (_resourcestrategy == ResourceStrategies.AutoRelease)
            {
                Release(Resource.Key);
            }
        }
        #endregion
    }
}
