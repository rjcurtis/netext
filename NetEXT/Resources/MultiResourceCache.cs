﻿using System;
using System.Collections.Generic;

namespace NetEXT.Resources
{
    public class MultiResourceCache
    {
        #region Variables
        private ResourceStrategies _resourcestrategy = ResourceStrategies.AutoRelease;
        private Dictionary<object, object> _resources = new Dictionary<object, object>();
        private DynamicDelegate _constructiondelegate = null;
        #endregion

        #region Functions
        public ResourceStrategies ResourceStrategy
        {
            get
            {
                return _resourcestrategy;
            }
            set
            {
                _resourcestrategy = value;
            }
        }
        public DynamicDelegate ConstructionDelegate
        {
            get
            {
                return _constructiondelegate;
            }
            set
            {
                _constructiondelegate = value;
            }
        }
        #endregion

        #region Contructors
        public MultiResourceCache(DynamicDelegate ConstructionDelegate, ResourceStrategies ResourceStrategy = ResourceStrategies.AutoRelease)
        {
            _resourcestrategy = ResourceStrategy;
            _constructiondelegate = ConstructionDelegate;
        }
        #endregion

        #region Functions
        public ResourceHandle<T> Aquire<T>(object Key) where T : IDisposable { return Aquire<T>(Key, new object[0]); }
        public ResourceHandle<T> Aquire<T>(object Key, object Parameter) where T : IDisposable { return Aquire<T>(Key, new object[] { Parameter }); }
        public ResourceHandle<T> Aquire<T>(object Key, object[] Parameters) where T : IDisposable
        {
            if (_resources.ContainsKey(Key)) return new ResourceHandle<T>((ResourceHolder<T>)_resources[Key]);
            else
            {
                ResourceHolder<T> newresource = new ResourceHolder<T>(ConstructionDelegate.InvokeDelegate<T>(Parameters));
                _resources.Add(Key, newresource);
                newresource.ResourceNotReferenced += ResourceNotReferenced;
                newresource.Key = Key;
                return new ResourceHandle<T>(newresource);
            }
        }
        public void Release<T>(object Key) where T : IDisposable
        {
            if (!_resources.ContainsKey(Key)) return;
            ((ResourceHolder<T>)_resources[Key]).DisposeResource();
            ((ResourceHolder<T>)_resources[Key]).ResourceNotReferenced -= ResourceNotReferenced;
            _resources.Remove(Key);
        }
        private void ResourceNotReferenced<T>(ResourceHolder<T> Resource) where T : IDisposable
        {
            if (_resourcestrategy == ResourceStrategies.AutoRelease)
            {
                Release<T>(Resource.Key);
            }
        }
        #endregion
    }
}
