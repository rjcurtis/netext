﻿using System;

namespace NetEXT.Resources
{
    public class ResourceHandle<T> where T : IDisposable
    {
        #region Variables
        private ResourceHolder<T> _resource = null;
        private bool _released = false;
        #endregion

        #region Properties
        public T Resource
        {
            get
            {
                if (_released) throw new Exception("The handle has already been released");
                return _resource.InternalResource;
            }
        }
        #endregion

        #region Constructors/Destructors
        internal ResourceHandle(ResourceHolder<T> Resource)
        {
            _resource = Resource;
            _resource.ReferenceCount += 1;
        }
        ~ResourceHandle()
        {
            ReleaseHandle();
        }
        public void ReleaseHandle()
        {
            if (!_released) _resource.ReferenceCount -= 1;
            _released = true;
        }
        #endregion

        #region Operators
        public static implicit operator T(ResourceHandle<T> Handle)
        {
            return Handle.Resource;
        }
        #endregion
    }
}
