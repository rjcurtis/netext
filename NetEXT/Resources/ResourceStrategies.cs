﻿using System;

namespace NetEXT.Resources
{
    public enum ResourceStrategies
    {
        AutoRelease,
        ExplicitRelease
    }
}
