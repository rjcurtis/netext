﻿using System;

namespace NetEXT.Resources
{
    public class ResourceHolder<T> where T : IDisposable
    {
        #region Variables
        private int _referencecount = 0;
        private bool _shoulddispose = false;
        private bool _isdisposed = false;
        private object _key = null;
        private T _resource = default(T);
        #endregion

        #region Properties
        internal int ReferenceCount
        {
            get
            {
                return _referencecount;
            }
            set
            {
                _referencecount = value;
                if (ReferenceCount == 0 && _shoulddispose) DisposeResource();
                if (ReferenceCount == 0 && ResourceNotReferenced != null) ResourceNotReferenced(this);
            }
        }
        internal object Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }
        internal T InternalResource
        {
            get
            {
                return _resource;
            }
        }
        #endregion

        #region Events
        internal event Action<ResourceHolder<T>> ResourceNotReferenced;
        #endregion

        #region Constructors
        public ResourceHolder(T Resource)
        {
            _resource = Resource;
        }
        #endregion

        #region Functions
        internal void DisposeResource()
        {
            if (_isdisposed) return;
            if (ReferenceCount == 0)
            {
                _isdisposed = true;
                _resource.Dispose();
            }
            else
            {
                _shoulddispose = true;
            }
        }
        #endregion
    }
}
