﻿using System;
using System.Collections.Generic;

namespace NetEXT
{
    public class DynamicDelegate
    {
        #region Variables
        private List<Delegate> _delegates = new List<Delegate>();
        #endregion

        #region Properties
        public Delegate[] Delegates
        {
            get
            {
                return _delegates.ToArray();
            }
        }
        #endregion

        #region Functions
        public void RegisterDelegate(Delegate Delegate)
        {
            _delegates.Add(Delegate);
        }
        public void UnregisterDelegate(Delegate Delegate)
        {
            _delegates.Remove(Delegate);
        }
        public object InvokeDelegate() { return InvokeDelegate(new object[0]); }
        public object InvokeDelegate(object Parameter) { return InvokeDelegate(new object[] { Parameter }); }
        public object InvokeDelegate(object[] Parameters)
        {
            foreach (var del in _delegates)
            {
                var args = del.Method.GetParameters();
                if (args.Length == Parameters.Length)
                {
                    bool matches = true;
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].ParameterType != Parameters[i].GetType())
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches) return del.DynamicInvoke(Parameters);
                }
            }
            throw new Exception("No matching delegate found for the provided Parameter(s)");
        }
        public T InvokeDelegate<T>() { return InvokeDelegate<T>(new object[0]); }
        public T InvokeDelegate<T>(object Parameter) { return InvokeDelegate<T>(new object[] { Parameter }); }
        public T InvokeDelegate<T>(object[] Parameters)
        {
            foreach (var del in _delegates)
            {
                var args = del.Method.GetParameters();
                if (args.Length == Parameters.Length && typeof(T) == del.Method.ReturnType)
                {
                    bool matches = true;
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (args[i].ParameterType != Parameters[i].GetType())
                        {
                            matches = false;
                            break;
                        }
                    }
                    if (matches) return (T)del.DynamicInvoke(Parameters);
                }
            }
            throw new Exception("No matching delegate found for the provided Parameter(s) and/or Return Type");
        }
        #endregion

        #region Operators
        public static DynamicDelegate operator +(DynamicDelegate LHS, DynamicDelegate RHS)
        {
            DynamicDelegate combined = new DynamicDelegate();
            foreach (var del in LHS.Delegates) { combined.RegisterDelegate(del); }
            foreach (var del in RHS.Delegates) { combined.RegisterDelegate(del); }
            return combined;
        }
        #endregion
    }
}
