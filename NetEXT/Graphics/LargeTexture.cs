﻿using System;
using System.Collections.Generic;
using System.IO;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Graphics
{
    public class LargeTexture : IDisposable
    {
        #region Variables
        private Texture[] _texturelist = null;
        private Vector2u[] _positionlist = null;
        private bool _issmoothed = false;
        private Vector2u _totalsize = new Vector2u(0, 0);
        private bool _isdisposed = false;
        #endregion

        #region Properties
        public bool Smooth
        {
            get
            {
                return _issmoothed;
            }
            set
            {
                _issmoothed = value;
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    _texturelist[i].Smooth = value;
                }
            }
        }
        public Vector2u Size
        {
            get
            {
                return _totalsize;
            }
        }
        internal Texture[] TextureList
        {
            get
            {
                return _texturelist;
            }
        }
        internal Vector2u[] PositionList
        {
            get
            {
                return _positionlist;
            }
        }
        #endregion

        #region Constructors/Destructors
        public LargeTexture(string filename)
        {
            Image img = new Image(filename);
            Create(img);
            img.Dispose();
            img = null;
        }
        public LargeTexture(Image image)
        {
            Create(image);
        }
        public LargeTexture(Stream stream)
        {
            Image img = new Image(stream);
            Create(img);
            img.Dispose();
            img = null;
        }
        public LargeTexture(uint width, uint height)
        {
            Image img = new Image(width, height);
            Create(img);
            img.Dispose();
            img = null;
        }
        public LargeTexture(uint width, uint height, byte[] pixels)
        {
            Image img = new Image(width, height, pixels);
            Create(img);
            img.Dispose();
            img = null;
        }
        public LargeTexture(Color[,] pixels)
        {
            Image img = new Image(pixels);
            Create(img);
            img.Dispose();
            img = null;
        }
        private void Create(Image CurrentImage)
        {
            List<Texture> newtextlist = new List<Texture>();
            List<Vector2u> newposlist = new List<Vector2u>();
            uint maxsize = Texture.MaximumSize;
            for (uint y = 0; y < CurrentImage.Size.Y; y += maxsize)
            {
                for (uint x = 0; x < CurrentImage.Size.X; x += maxsize)
                {
                    Texture newtexture = new Texture(CurrentImage, new IntRect((int)x, (int)y, (int)Math.Min(maxsize, CurrentImage.Size.X - x), (int)Math.Min(maxsize, CurrentImage.Size.Y - y)));
                    newtexture.Repeated = false;
                    newtexture.Smooth = _issmoothed;
                    newtextlist.Add(newtexture);
                    newposlist.Add(new Vector2u(x, y));
                }
            }
            _texturelist = newtextlist.ToArray();
            _positionlist = newposlist.ToArray();
            _totalsize = CurrentImage.Size;
        }
        internal LargeTexture(LargeRenderTexture CurrentRenderTexture)
        {
            _texturelist = new Texture[CurrentRenderTexture.RenderTextureList.Length];
            for (int i = 0; i < CurrentRenderTexture.RenderTextureList.Length; i++)
            {
                _texturelist[i] = new Texture(CurrentRenderTexture.RenderTextureList[i].Texture);
            }
            _totalsize = CurrentRenderTexture.Size;
            _positionlist = CurrentRenderTexture.PositionList;
        }
        public void Dispose()
        {
            if (!_isdisposed)
            {
                _isdisposed = true;
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    _texturelist[i].Dispose();
                    _texturelist[i] = null;
                }
                _texturelist = null;
            }
        }
        #endregion

        #region Functions
        public void Update(byte[] pixels)
        {
            if (_texturelist.Length > 1)
            {
                for (int i = 0; i < _texturelist.Length; i++)
                {
                    byte[] subpixels = new byte[_texturelist[i].Size.X * _texturelist[i].Size.Y * 4];
                    for (int y = 0; y < _texturelist[i].Size.Y; y++)
                    {
                        Array.Copy(pixels, (_positionlist[i].X * 4) + (((y + _positionlist[i].Y) * _totalsize.X * 4)), subpixels, _texturelist[i].Size.X * y * 4, _texturelist[i].Size.X * 4);
                    }
                    _texturelist[i].Update(subpixels, _texturelist[i].Size.X, _texturelist[i].Size.Y, 0, 0);
                }
            }
            else if (_texturelist.Length == 1)
            {
                _texturelist[0].Update(pixels, _totalsize.X, _totalsize.Y, 0, 0);
            }
        }
        #endregion
    }
}
