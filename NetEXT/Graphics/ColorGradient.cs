﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Graphics
{
    public class ColorGradient
    {
        #region Variables
        private Dictionary<float, Color> _colors = new Dictionary<float, Color>();
        private List<float> _keys = new List<float>();
        #endregion

        #region Properties
        public Color this[float Position]
        {
            get
            {
                return _colors[Position];
            }
            set
            {
                if (_keys.Contains(Position))
                {
                    _colors[Position] = value;
                }
                else
                {
                    _colors.Add(Position, value);
                    _keys.Add(Position);
                    _keys.Sort();
                }
            }
        }
        #endregion

        #region Functions
        public static Color BlendColors(Color FirstColor, Color SecondColor, float Interpolation)
        {
            float firstpart = 1f - Interpolation;
            return new Color(
                (byte)((firstpart * (float)FirstColor.R) + (Interpolation * (float)SecondColor.R)),
                (byte)((firstpart * (float)FirstColor.G) + (Interpolation * (float)SecondColor.G)),
                (byte)((firstpart * (float)FirstColor.B) + (Interpolation * (float)SecondColor.B)),
                (byte)((firstpart * (float)FirstColor.A) + (Interpolation * (float)SecondColor.A)));
        }
        public Color GetColor(float Position)
        {
            float nextcolor = 0;
            float prevcolor = 1;
            for (int i = 0; i < _keys.Count; i++)
            {
                if (_keys[i] == Position) return _colors[_keys[i]];
                else if (_keys[i] > Position)
                {
                    nextcolor = _keys[i];
                    if (i > 0) prevcolor = _keys[i - 1];
                    else prevcolor = _keys[_keys.Count - 1];
                    break;
                }
            }
            float interpolation = (Position - prevcolor) / (nextcolor - prevcolor);
            return BlendColors(_colors[prevcolor], _colors[nextcolor], interpolation);
        }
        #endregion
    }
}
