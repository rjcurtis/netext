﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Graphics
{
    public class LargeSprite : Transformable, Drawable
    {
        #region Variables
        private LargeTexture _currenttexture = null;
        private Sprite[] _spritelist = new Sprite[0];
        private Color _spritecolor = Color.White;
        #endregion

        #region Properties
        public Color Color
        {
            get
            {
                return _spritecolor;
            }
            set
            {
                _spritecolor = value;
                for (int i = 0; i < _spritelist.Length; i++)
                {
                    _spritelist[i].Color = _spritecolor;
                }
            }
        }
        public LargeTexture Texture
        {
            get
            {
                return _currenttexture;
            }
            set
            {
                _currenttexture = value;
                UpdateSpriteList();
            }
        }
        #endregion

        #region Constructors/Destructors
        public LargeSprite(LargeTexture CurrentTexture)
        {
            _currenttexture = CurrentTexture;
            UpdateSpriteList();
        }
        #endregion

        #region Functions
        private void UpdateSpriteList()
        {
            for (int i = 0; i < _spritelist.Length; i++)
            {
                _spritelist[i].Dispose();
                _spritelist[i] = null;
            }
            _spritelist = new Sprite[_currenttexture.TextureList.Length];
            for (int i = 0; i < _spritelist.Length; i++)
            {
                _spritelist[i] = new Sprite(_currenttexture.TextureList[i]);
                _spritelist[i].Position = new Vector2f(_currenttexture.PositionList[i].X, _currenttexture.PositionList[i].Y);
                _spritelist[i].Color = _spritecolor;
            }
        }
        public void Draw(RenderTarget target, RenderStates states)
        {
            states.Transform = base.Transform;
            for (int i = 0; i < _spritelist.Length; i++)
            {
                target.Draw(_spritelist[i], states);
            }
        }
        public FloatRect GetLocalBounds()
        {
            return new FloatRect(0, 0, _currenttexture.Size.X, _currenttexture.Size.Y);
        }
        public FloatRect GetGlobalBounds()
        {
            return base.Transform.TransformRect(GetLocalBounds());
        }
        #endregion
    }
}
