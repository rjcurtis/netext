﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Graphics
{
    public class LargeRenderTexture : IDisposable, RenderTarget
    {
        #region Variables
        private RenderTexture[] _rendertexturelist = null;
        private Vector2u[] _positionlist = null;
        private bool _issmoothed = false;
        private Vector2u _totalsize = new Vector2u(0, 0);
        private View _currentview = null;
        private bool _isdisposed = false;
        #endregion

        #region Properties
        public bool Smooth
        {
            get
            {
                return _issmoothed;
            }
            set
            {
                _issmoothed = value;
                for (int i = 0; i < _rendertexturelist.Length; i++)
                {
                    _rendertexturelist[i].Smooth = value;
                }
            }
        }
        public Vector2u Size
        {
            get
            {
                return _totalsize;
            }
        }
        public View DefaultView
        {
            get
            {
                return new View(new FloatRect(0, 0, _totalsize.X, _totalsize.Y));
            }
        }
        public LargeTexture Texture
        {
            get
            {
                return new LargeTexture(this);
            }
        }
        internal RenderTexture[] RenderTextureList
        {
            get
            {
                return _rendertexturelist;
            }
        }
        internal Vector2u[] PositionList
        {
            get
            {
                return _positionlist;
            }
        }
        #endregion

        #region Contructors/Destructors
        public LargeRenderTexture(uint Width, uint Height) : this(Width, Height, false) { }
        public LargeRenderTexture(uint Width, uint Height, bool UseDepthBuffer)
        {
            List<RenderTexture> newrendtextlist = new List<RenderTexture>();
            List<Vector2u> newposlist = new List<Vector2u>();
            _currentview = new View(new FloatRect(0, 0, Width, Height));
            uint maxsize = SFML.Graphics.Texture.MaximumSize;
            for (uint y = 0; y < Height; y += maxsize)
            {
                for (uint x = 0; x < Width; x += maxsize)
                {
                    RenderTexture newrendtexture = new RenderTexture((uint)Math.Min(maxsize, Width - x), (uint)Math.Min(maxsize, Height - y), UseDepthBuffer);
                    newrendtexture.Smooth = _issmoothed;
                    newrendtextlist.Add(newrendtexture);
                    newposlist.Add(new Vector2u(x, y));
                }
            }
            _rendertexturelist = newrendtextlist.ToArray();
            _positionlist = newposlist.ToArray();
            _totalsize = new Vector2u(Width, Height);
        }
        public void Dispose()
        {
            if (!_isdisposed)
            {
                _isdisposed = true;
                for (int i = 0; i < _rendertexturelist.Length; i++)
                {
                    _rendertexturelist[i].Dispose();
                    _rendertexturelist[i] = null;
                }
                _rendertexturelist = null;
            }
        }
        #endregion

        #region Functions
        public void Clear()
        {
            Clear(Color.Black);
        }
        public void Clear(Color color)
        {
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                _rendertexturelist[i].Clear(color);
            }
        }
        public void Draw(Drawable drawable)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(drawable, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }

        #region Draw Overloads
        public void Draw(Drawable drawable, RenderStates states)
        {
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(drawable, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }
        public void Draw(Vertex[] vertices, PrimitiveType type)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(vertices, type, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }
        public void Draw(Vertex[] vertices, PrimitiveType type, RenderStates states)
        {
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(vertices, type, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type)
        {
            RenderStates states = RenderStates.Default;
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(vertices, start, count, type, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }
        public void Draw(Vertex[] vertices, uint start, uint count, PrimitiveType type, RenderStates states)
        {
            Transform transform = states.Transform;
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                transform.Translate(-_positionlist[i].X, -_positionlist[i].Y);
                states.Transform = transform;
                _rendertexturelist[i].Draw(vertices, start, count, type, states);
                transform.Translate(_positionlist[i].X, _positionlist[i].Y);
                states.Transform = transform;
            }
        }
        #endregion

        public View GetView()
        {
            return _currentview;
        }
        public IntRect GetViewport(View view)
        {
            FloatRect rec = view.Viewport;
            return new IntRect((int)rec.Left, (int)rec.Top, (int)rec.Width, (int)rec.Height);
        }
        public Vector2i MapCoordsToPixel(Vector2f point)
        {
            return MapCoordsToPixel(point, _currentview);
        }
        public Vector2i MapCoordsToPixel(Vector2f point, View view)
        {
            throw new Exception("'MapCoordsToPixel' is not implemented");
        }
        public Vector2f MapPixelToCoords(Vector2i point)
        {
            return MapPixelToCoords(point, _currentview);
        }
        public Vector2f MapPixelToCoords(Vector2i point, View view)
        {
            throw new Exception("'MapPixelToCoords' is not implemented");
        }
        public void PopGLStates()
        {
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                _rendertexturelist[i].PopGLStates();
            }
        }
        public void PushGLStates()
        {
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                _rendertexturelist[i].PushGLStates();
            }
        }
        public void ResetGLStates()
        {
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                _rendertexturelist[i].ResetGLStates();
            }
        }
        public void SetView(View view)
        {
            throw new Exception("'SetView' is not implemented");
        }
        public void Display()
        {
            for (int i = 0; i < _rendertexturelist.Length; i++)
            {
                _rendertexturelist[i].Display();
            }
        }
        #endregion
    }
}
