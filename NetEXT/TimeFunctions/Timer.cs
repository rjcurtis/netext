﻿using System;

namespace NetEXT.TimeFunctions
{
    public class Timer
    {
        #region Variables
        private StopWatch _stopwatch = new StopWatch();
        private Time _limit = Time.Zero;
        #endregion

        #region Properties
        public Time RemainingTime
        {
            get
            {
                Time remaining = _limit - _stopwatch.ElapsedTime;
                if (remaining > Time.Zero) return remaining;
                else return Time.Zero;
            }
        }
        public bool IsRunning
        {
            get
            {
                return _stopwatch.IsRunning && !IsExpired;
            }
        }
        public bool IsExpired
        {
            get
            {
                return _stopwatch.ElapsedTime >= _limit;
            }
        }
        #endregion

        #region Functions
        public virtual void Start()
        {
            _stopwatch.Start();
        }
        public virtual void Stop()
        {
            _stopwatch.Stop();
        }
        public virtual void Reset(Time TimeLimit)
        {
            _limit = TimeLimit;
            _stopwatch.Reset();
        }
        public virtual void Restart(Time TimeLimit)
        {
            Reset(TimeLimit);
            Start();
        }
        #endregion
    }
}
