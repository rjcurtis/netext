﻿using System;

namespace NetEXT.TimeFunctions
{
    public class StopWatch
    {
        #region Variables
        private Time _timebuffer = Time.Zero;
        private Clock _clock = new Clock();
        private bool _isrunning = false;
        #endregion

        #region Properties
        public Time ElapsedTime
        {
            get
            {
                if (_isrunning)
                {
                    return _timebuffer + _clock.ElapsedTime;
                }
                else
                {
                    return _timebuffer;
                }
            }
        }
        public bool IsRunning
        {
            get
            {
                return _isrunning;
            }
        }
        #endregion

        #region Functions
        public void Start()
        {
            if (!_isrunning)
            {
                _isrunning = true;
                _clock.Restart();
            }
        }
        public void Stop()
        {
            if (_isrunning)
            {
                _isrunning = false;
                _timebuffer += _clock.ElapsedTime;
            }
        }
        public void Reset()
        {
            _timebuffer = Time.Zero;
            _isrunning = false;
            _clock.Restart();
        }
        public void Restart()
        {
            Reset();
            Start();
        }
        #endregion
    }
}
