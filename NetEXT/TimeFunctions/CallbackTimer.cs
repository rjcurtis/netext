﻿using System;
using NetEXT.Input;
using NetEXT.TimeFunctions;

namespace NetEXT.TimeFunctions
{
    public class CallbackTimer : Timer
    {
        #region Variables
        private ListenerSequence<CallbackTimer> _listeners = new ListenerSequence<CallbackTimer>();
        private bool _justexpired = true;
        #endregion

        #region Functions
        public override void Reset(Time TimeLimit)
        {
            base.Reset(TimeLimit);
            _justexpired = false;
        }
        public override void Restart(Time TimeLimit)
        {
            base.Restart(TimeLimit);
            _justexpired = false;
        }
        public void Update()
        {
            if (base.IsExpired && !_justexpired)
            {
                _justexpired = true;
                _listeners.Call(this);
            }
        }
        public Connection Connect(EventSystem<CallbackTimer>.Function Listener)
        {
            return _listeners.Add(new Listener<CallbackTimer>(Listener));
        }
        public void ClearConnections()
        {
            _listeners.Clear();
        }
        #endregion
    }
}
