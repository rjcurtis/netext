﻿using System;

namespace NetEXT.Input
{
    public enum ActionType
    {
        Hold,
        PressOnce,
        ReleaseOnce
    }
}
