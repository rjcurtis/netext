﻿using System;
using System.Collections.Generic;

namespace NetEXT.Input
{
    public class Connection
    {
        #region Variables
        private WeakReference _connection = null;
        #endregion

        #region Properties
        public bool Connected
        {
            get
            {
                return _connection.Target != null;
            }
        }
        #endregion

        #region Constructors/Destructors
        internal Connection(ConnectionBase Tracker)
        {
            _connection = new WeakReference(Tracker);
        }
        #endregion

        #region Functions
        public void Disconnect()
        {
            if (_connection.Target != null)
            {
                ((ConnectionBase)_connection.Target).Disconnect();
            }
            _connection.Target = null;
        }
        #endregion
    }
    internal abstract class ConnectionBase
    {
        public abstract void Disconnect();
    }
    internal class ConnectionIterator<ParameterType> : ConnectionBase
    {
        #region Variables
        private ListenerIteratorRemoval<ParameterType> _container = null;
        private Listener<ParameterType> _listener = null;
        #endregion

        #region Constructors/Destructors
        public ConnectionIterator(ListenerIteratorRemoval<ParameterType> Container, Listener<ParameterType> Listener)
        {
            _container = Container;
            _listener = Listener;
        }
        #endregion

        #region Functions
        public override void Disconnect()
        {
            _container.Remove(_listener);
        }
        #endregion
    }
}
