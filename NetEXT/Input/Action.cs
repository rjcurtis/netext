﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    public class Action
    {
        #region Variables
        private ActionNode _operation = null;
        #endregion

        #region Constructors/Destructors
        public Action(Keyboard.Key Key) : this(Key, ActionType.Hold) { }
        public Action(Keyboard.Key Key, ActionType Action)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeKeyLeaf(Key);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventKeyLeaf(Key, Action == ActionType.PressOnce);
                    break;
            }
        }
        public Action(Mouse.Button MouseButton) : this(MouseButton, ActionType.Hold) { }
        public Action(Mouse.Button MouseButton, ActionType Action)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeMouseLeaf(MouseButton);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventMouseLeaf(MouseButton, Action == ActionType.PressOnce);
                    break;
            }
        }
        public Action(NetEXT.Input.Joystick Joystick) : this(Joystick, ActionType.Hold) { }
        public Action(NetEXT.Input.Joystick Joystick, ActionType Action)
        {
            switch (Action)
            {
                case ActionType.Hold:
                    _operation = new RealtimeJoystickLeaf(Joystick);
                    break;
                case ActionType.PressOnce:
                case ActionType.ReleaseOnce:
                    _operation = new EventJoystickLeaf(Joystick, Action == ActionType.PressOnce);
                    break;
            }
        }
        public Action(EventType EventType)
        {
            _operation = new MiscEventLeaf(EventType);
        }
        private Action(ActionNode Operation)
        {
            _operation = Operation;
        }
        #endregion

        #region Functions
        public bool IsActive(EventBuffer Buffer)
        {
            return _operation.IsActionActive(Buffer);
        }
        public bool IsActive(EventBuffer Buffer, ActionResult Result)
        {
            return _operation.IsActionActive(Buffer, Result);
        }
        #endregion

        #region Operators
        public static Action operator |(Action LHS, Action RHS)
        {
            return new Action(new OrNode(LHS._operation, RHS._operation));
        }
        public static Action operator &(Action LHS, Action RHS)
        {
            return new Action(new AndNode(LHS._operation, RHS._operation));
        }
        public static Action operator !(Action Action)
        {
            return new Action(new NotNode(Action._operation));
        }
        #endregion
    }
}
