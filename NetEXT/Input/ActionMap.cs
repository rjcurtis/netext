﻿using System;
using System.Collections.Generic;
using SFML.Window;

namespace NetEXT.Input
{
    public class ActionMap
    {
        #region Variables
        private Dictionary<object, Action> _actionmap = new Dictionary<object, Action>();
        private EventBuffer _eventbuffer = new EventBuffer();
        #endregion

        #region Properties
        public Action this[object ID]
        {
            get
            {
                if (_actionmap.ContainsKey(ID)) return _actionmap[ID];
                else return null;
            }
            set
            {
                if (_actionmap.ContainsKey(ID)) _actionmap[ID] = value;
                else _actionmap.Add(ID, value);
            }
        }
        #endregion

        #region Functions
        public static EventSystem<ActionContext> CreateCallbackSystem()
        {
            return new EventSystem<ActionContext>() { GetEventID = (evt) => { return evt.ActionID; } };
        }
        public void Update(Window Window)
        {
            _eventbuffer.ClearEvents();
            _eventbuffer.PollEvents(Window);
        }
        public void PushEvent(Event Event)
        {
            _eventbuffer.PushEvent(Event);
        }
        public void ClearEvents()
        {
            _eventbuffer.ClearEvents();
        }
        public void RemoveAction(object ID)
        {
            _actionmap.Remove(ID);
        }
        public bool IsActive(object ID)
        {
            if (!_actionmap.ContainsKey(ID)) return false;
            return _actionmap[ID].IsActive(_eventbuffer);
        }
        public void InvokeCallbacks(EventSystem<ActionContext> System, Window Window)
        {
            foreach (var action in _actionmap)
            {
                ActionResult result = new ActionResult();
                if (!action.Value.IsActive(_eventbuffer, result)) continue;
                if (result.RealtimeTriggers > 0) System.TriggerEvent(new ActionContext(Window, null, action.Key));
                foreach (Event evt in result.EventContainer)
                {
                    System.TriggerEvent(new ActionContext(Window, evt, action.Key));
                }
            }
        }
        #endregion
    }
}
