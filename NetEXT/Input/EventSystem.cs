﻿using System;
using System.Collections.Generic;
using SFML.Window;

namespace NetEXT.Input
{
    public class EventSystem<ParameterType>
    {
        #region Delegates
        public delegate object GetEventIDDelegate(ParameterType Event);
        public delegate void Function(ParameterType Parameter);
        #endregion

        #region Variables
        private ListenerMap<ParameterType> _listeners = new ListenerMap<ParameterType>();
        private GetEventIDDelegate _geteventid = new GetEventIDDelegate((ParameterType obj) => { return obj; });
        #endregion

        #region Properties
        public GetEventIDDelegate GetEventID
        {
            get
            {
                return _geteventid;
            }
            set
            {
                _geteventid = value;
            }
        }
        #endregion

        #region Functions
        public void TriggerEvent(ParameterType Event)
        {
            if (_geteventid == null) return;
            _listeners.Call(_geteventid(Event), Event);
        }
        public Connection Connect(object Trigger, Function Listener)
        {
            return _listeners.Add(Trigger, new Listener<ParameterType>(Listener));
        }
        public void ClearConnections(object Identifier)
        {
            _listeners.Clear(Identifier);
        }
        public void ClearAllConnections()
        {
            _listeners.ClearAll();
        }
        #endregion
    }
}
