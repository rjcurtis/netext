﻿using System;

namespace NetEXT.Input
{
    public struct Joystick
    {
        public uint ID;
        public uint Button;
        public Joystick(uint NewID, uint NewButton)
        {
            ID = NewID;
            Button = NewButton;
        }
    }
    public struct Joy
    {
        public uint ID;
        public Joy(uint NewID)
        {
            ID = NewID;
        }
        public Joystick Button(uint Button)
        {
            return new Joystick(ID, Button);
        }
    }
}
