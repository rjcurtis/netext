﻿using System;
using System.Collections.Generic;

namespace NetEXT.Input
{
    internal interface ListenerIteratorRemoval<ParameterType>
    {
        void Remove(Listener<ParameterType> Listener);
    }
    internal class Listener<ParameterType>
    {
        #region Variables
        private EventSystem<ParameterType>.Function _function = null;
        private ListenerIteratorRemoval<ParameterType> _container = null;
        #endregion

        #region Constructors/Destructors
        public Listener(EventSystem<ParameterType>.Function Function)
        {
            _function = Function;
        }
        #endregion

        #region Functions
        public void Call(ParameterType Arg)
        {
            _function(Arg);
        }
        public void SetEnviroment(ListenerIteratorRemoval<ParameterType> Container)
        {
            _container = Container;
        }
        public Connection ShareConnection()
        {
            return new Connection(new ConnectionIterator<ParameterType>(_container, this));
        }
        #endregion
    }
    internal class ListenerSequence<ParameterType> : ListenerIteratorRemoval<ParameterType>
    {
        #region Variables
        private List<Listener<ParameterType>> _container = new List<Listener<ParameterType>>();
        #endregion

        #region Functions
        public Connection Add(Listener<ParameterType> Listener)
        {
            _container.Add(Listener);
            Listener.SetEnviroment(this);
            return Listener.ShareConnection();
        }
        public void Remove(Listener<ParameterType> Listener)
        {
            _container.Remove(Listener);
        }
        public void Clear()
        {
            _container.Clear();
        }
        public void Call(ParameterType Arg)
        {
            foreach (Listener<ParameterType> listener in _container)
            {
                listener.Call(Arg);
            }
        }
        #endregion
    }
    internal class ListenerMap<ParameterType> : ListenerIteratorRemoval<ParameterType>
    {
        #region Variables
        private Dictionary<object, List<Listener<ParameterType>>> _container = new Dictionary<object, List<Listener<ParameterType>>>();
        #endregion

        #region Functions
        public Connection Add(object Trigger, Listener<ParameterType> Listener)
        {
            if (!_container.ContainsKey(Trigger)) _container.Add(Trigger, new List<Listener<ParameterType>>());
            _container[Trigger].Add(Listener);
            Listener.SetEnviroment(this);
            return Listener.ShareConnection();
        }
        public void Remove(Listener<ParameterType> Listener)
        {
            foreach (var triggerlistener in _container)
            {
                if (triggerlistener.Value.Remove(Listener)) break;
            }
        }
        public void Clear(object Key)
        {
            _container.Remove(Key);
        }
        public void ClearAll()
        {
            _container.Clear();
        }
        public void Call(object Event, ParameterType Arg)
        {
            if (!_container.ContainsKey(Event)) return;
            foreach (Listener<ParameterType> Listener in _container[Event])
            {
                Listener.Call(Arg);
            }
        }
        #endregion
    }
}
