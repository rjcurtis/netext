﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    public struct ActionContext
    {
        public Window Window;
        public Event? Event;
        public object ActionID;
        public ActionContext(Window NewWindow, Event? NewEvent, object NewActionID)
        {
            Window = NewWindow;
            Event = NewEvent;
            ActionID = NewActionID;
        }
    }
}
