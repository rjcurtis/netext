﻿using System;
using SFML.Window;

namespace NetEXT.Input
{
    public abstract class ActionNode
    {
        public abstract bool IsActionActive(EventBuffer Buffer);
        public abstract bool IsActionActive(EventBuffer Buffer, ActionResult Result);
        public virtual bool FilterOut(Event Event)
        {
            return false;
        }
    }
    public abstract class RealtimeNode : ActionNode
    {
        public override abstract bool IsActionActive(EventBuffer Buffer);
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            if (IsActionActive(Buffer))
            {
                Result.RealtimeTriggers += 1;
                return true;
            }
            else return false;
        }
    }
    public class RealtimeKeyLeaf : RealtimeNode
    {
        private Keyboard.Key _key;
        public RealtimeKeyLeaf(Keyboard.Key Key)
        {
            _key = Key;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.RealtimeInputEnabled && Keyboard.IsKeyPressed(_key);
        }
    }
    public class EventKeyLeaf : ActionNode
    {
        private Event _keyevent;
        public EventKeyLeaf(Keyboard.Key Key, bool Pressed)
        {
            _keyevent.Type = Pressed ? EventType.KeyPressed : EventType.KeyReleased;
            _keyevent.Key.Code = Key;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.ContainsEvent(_keyevent, this);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            return Buffer.GetEvents(_keyevent.Type, Result.EventContainer, this);
        }
        public override bool FilterOut(Event Event)
        {
            return Event.Type == _keyevent.Type && Event.Key.Code == _keyevent.Key.Code;
        }
    }
    public class RealtimeMouseLeaf : RealtimeNode
    {
        private Mouse.Button _button;
        public RealtimeMouseLeaf(Mouse.Button MouseButton)
        {
            _button = MouseButton;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.RealtimeInputEnabled && Mouse.IsButtonPressed(_button);
        }
    }
    public class EventMouseLeaf : ActionNode
    {
        private Event _mouseevent;
        public EventMouseLeaf(Mouse.Button MouseButton, bool Pressed)
        {
            _mouseevent.Type = Pressed ? EventType.MouseButtonPressed : EventType.MouseButtonReleased;
            _mouseevent.MouseButton.Button = MouseButton;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.ContainsEvent(_mouseevent, this);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            return Buffer.GetEvents(_mouseevent.Type, Result.EventContainer, this);
        }
        public override bool FilterOut(Event Event)
        {
            return Event.Type == _mouseevent.Type && Event.MouseButton.Button == _mouseevent.MouseButton.Button;
        }
    }
    public class RealtimeJoystickLeaf : RealtimeNode
    {
        private NetEXT.Input.Joystick _joystick;
        public RealtimeJoystickLeaf(NetEXT.Input.Joystick Joystick)
        {
            _joystick = Joystick;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.RealtimeInputEnabled && SFML.Window.Joystick.IsButtonPressed(_joystick.ID, _joystick.Button);
        }
    }
    public class EventJoystickLeaf : ActionNode
    {
        private Event _joystickevent;
        public EventJoystickLeaf(NetEXT.Input.Joystick Joystick, bool Pressed)
        {
            _joystickevent.Type = Pressed ? EventType.JoystickButtonPressed : EventType.JoystickButtonReleased;
            _joystickevent.JoystickButton.JoystickId = Joystick.ID;
            _joystickevent.JoystickButton.Button = Joystick.Button;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.ContainsEvent(_joystickevent, this);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            return Buffer.GetEvents(_joystickevent.Type, Result.EventContainer, this);
        }
        public override bool FilterOut(Event Event)
        {
            return Event.Type == _joystickevent.Type && Event.JoystickButton.Button == _joystickevent.JoystickButton.Button;
        }
    }
    public class MiscEventLeaf : ActionNode
    {
        private Event _event;
        public MiscEventLeaf(EventType EventType)
        {
            _event.Type = EventType;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return Buffer.ContainsEvent(_event);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            return Buffer.GetEvents(_event.Type, Result.EventContainer);
        }
    }
    public class OrNode : ActionNode
    {
        private ActionNode _lhs;
        private ActionNode _rhs;
        public OrNode(ActionNode LHS, ActionNode RHS)
        {
            _lhs = LHS;
            _rhs = RHS;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return _lhs.IsActionActive(Buffer) || _rhs.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            bool lhsres = _lhs.IsActionActive(Buffer, Result);
            bool rhsres = _rhs.IsActionActive(Buffer, Result);
            return lhsres || rhsres;
        }
    }
    public class AndNode : ActionNode
    {
        private ActionNode _lhs;
        private ActionNode _rhs;
        public AndNode(ActionNode LHS, ActionNode RHS)
        {
            _lhs = LHS;
            _rhs = RHS;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return _lhs.IsActionActive(Buffer) && _rhs.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            ActionResult tmpresult = new ActionResult();
            if (_lhs.IsActionActive(Buffer, tmpresult) && _rhs.IsActionActive(Buffer, tmpresult))
            {
                Result.EventContainer.AddRange(tmpresult.EventContainer);
                Result.RealtimeTriggers += tmpresult.RealtimeTriggers;
                return true;
            }
            else return false;
        }
    }
    public class NotNode : ActionNode
    {
        private ActionNode _action;
        public NotNode(ActionNode Action)
        {
            _action = Action;
        }
        public override bool IsActionActive(EventBuffer Buffer)
        {
            return !_action.IsActionActive(Buffer);
        }
        public override bool IsActionActive(EventBuffer Buffer, ActionResult Result)
        {
            ActionResult tmpresult = new ActionResult();
            if (!_action.IsActionActive(Buffer, tmpresult))
            {
                Result.EventContainer.AddRange(tmpresult.EventContainer);
                Result.RealtimeTriggers += tmpresult.RealtimeTriggers;
                return true;
            }
            else return false;
        }
    }
}
