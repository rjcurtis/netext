﻿using System;
using System.Threading;

namespace NetEXT.Concurrent
{
    public abstract class Lockable
    {
        #region Variables
        private Mutex _lockmutex = new Mutex(false);
        #endregion

        #region Functions
        public void Lock()
        {
            _lockmutex.WaitOne();
        }
        public void ReleaseLock()
        {
            _lockmutex.ReleaseMutex();
        }
        #endregion
    }
}
