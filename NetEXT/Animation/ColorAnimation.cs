﻿using System;
using NetEXT.Graphics;

namespace NetEXT.Animation
{
    public class ColorAnimation<T> : AnimationBase<T>
    {
        #region Variables
        private ColorGradient _gradient = null;
        #endregion

        #region Properties
        private ColorGradient ColorGradient
        {
            get
            {
                return _gradient;
            }
            set
            {
                _gradient = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public ColorAnimation(ColorGradient NewColorGradient)
        {
            _gradient = NewColorGradient;
        }
        #endregion

        #region Functions
        public override void Animate(AnimatedObject<T> AnimatedObject, float Progress)
        {
            AnimatedObject.Color = _gradient.GetColor(Progress);
        }
        #endregion
    }
}
