﻿using System;

namespace NetEXT.Animation
{
    public class FadeAnimation<T> : AnimationBase<T>
    {
        #region Variables
        private float _fadeinratio = 0;
        private float _fadeoutratio = 0;
        #endregion

        #region Properties
        public float FadeInRatio
        {
            get
            {
                return _fadeinratio;
            }
            set
            {
                _fadeinratio = value;
            }
        }
        public float FadeOutRatio
        {
            get
            {
                return _fadeoutratio;
            }
            set
            {
                _fadeoutratio = value;
            }
        }
        #endregion

        #region Constructors/Destructors
        public FadeAnimation(float NewFadeInRatio, float NewFadeOutRatio)
        {
            _fadeinratio = NewFadeInRatio;
            _fadeoutratio = NewFadeOutRatio;
        }
        #endregion

        #region Functions
        public override void Animate(AnimatedObject<T> AnimatedObject, float Progress)
        {
            if (Progress < _fadeinratio)
            {
                AnimatedObject.Color = new SFML.Graphics.Color(AnimatedObject.Color.R, AnimatedObject.Color.G, AnimatedObject.Color.B, (byte)(256f * Progress / _fadeinratio));
            }
            else if (Progress > 1f - _fadeoutratio)
            {
                AnimatedObject.Color = new SFML.Graphics.Color(AnimatedObject.Color.R, AnimatedObject.Color.G, AnimatedObject.Color.B, (byte)(256f * (1f - Progress) / _fadeoutratio));
            }
        }
        #endregion
    }
}
