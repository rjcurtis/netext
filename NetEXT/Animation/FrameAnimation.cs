﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;

namespace NetEXT.Animation
{
    public class FrameAnimation<T> : AnimationBase<T>
    {
        #region Variables
        private List<Frame> _frames = new List<Frame>();
        private bool _isnormalized = false;
        #endregion

        #region Functions
        public void AddFrame(float RelativeDuration, IntRect TextureRect)
        {
            _frames.Add(new Frame(RelativeDuration, TextureRect));
            _isnormalized = false;
        }
        public void ClearFrames()
        {
            _frames.Clear();
            _isnormalized = false;
        }
        public override void Animate(AnimatedObject<T> AnimatedObject, float Progress)
        {
            if (!(_frames.Count > 0)) return;
            Normalize();
            float prog = Progress;
            foreach (Frame frame in _frames)
            {
                prog -= frame.Duration;
                if (prog < 0)
                {
                    AnimatedObject.TextureRect = frame.SubRect;
                    break;
                }
            }
        }
        private void Normalize()
        {
            if (_isnormalized) return;
            float sum = 0;
            foreach (Frame frame in _frames)
            {
                sum += frame.Duration;
            }
            foreach (Frame frame in _frames)
            {
                frame.Duration /= sum;
            }
            _isnormalized = true;
        }
        #endregion

        #region Additional Definitions
        private class Frame
        {
            #region Variables
            private float _duration = 0;
            private IntRect _subrect = new IntRect(0, 0, 1, 1);
            #endregion

            #region Properties
            public float Duration
            {
                get
                {
                    return _duration;
                }
                set
                {
                    _duration = value;
                }
            }
            public IntRect SubRect
            {
                get
                {
                    return _subrect;
                }
            }
            #endregion

            #region Constructors/Destructors
            public Frame(float NewDuration, IntRect NewSubRect)
            {
                _duration = NewDuration;
                _subrect = NewSubRect;
            }
            #endregion
        }
        #endregion
    }
}
