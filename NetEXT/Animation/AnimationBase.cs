﻿using System;

namespace NetEXT.Animation
{
    public abstract class AnimationBase<T>
    {
        public abstract void Animate(AnimatedObject<T> AnimatedObject, float Progress);
    }
}
