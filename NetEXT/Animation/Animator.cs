﻿using System;
using System.Collections.Generic;
using NetEXT.TimeFunctions;
using SFML.Window;

namespace NetEXT.Animation
{
    public class Animator<T>
    {
        #region Variables
        private Dictionary<object, AnimationBase<T>> _animationlist = new Dictionary<object, AnimationBase<T>>();
        private Dictionary<object, Time> _animationdurationlist = new Dictionary<object, Time>();
        private Dictionary<object, Time> _playinganimationdurationlist = new Dictionary<object, Time>();
        private Dictionary<object, bool> _playinganimationlooplist = new Dictionary<object, bool>();
        #endregion

        #region Properties
        public object[] AnimationsPlaying
        {
            get
            {
                object[] keys = new object[_playinganimationdurationlist.Keys.Count];
                _playinganimationdurationlist.Keys.CopyTo(keys, 0);
                return keys;
            }
        }
        #endregion

        #region Functions
        public void AddAnimation(object AnimationID, AnimationBase<T> Animation, Time Duration)
        {
            if (_animationlist.ContainsKey(AnimationID)) return;
            _animationlist.Add(AnimationID, Animation);
            _animationdurationlist.Add(AnimationID, Duration);
        }
        public void RemoveAnimation(object AnimationID)
        {
            if (_animationlist.ContainsKey(AnimationID))
            {
                _animationlist.Remove(AnimationID);
                _animationdurationlist.Remove(AnimationID);
            }
            if (_playinganimationdurationlist.ContainsKey(AnimationID))
            {
                _playinganimationdurationlist.Remove(AnimationID);
                _playinganimationlooplist.Remove(AnimationID);
            }
        }
        public void PlayAnimation(object AnimationID)
        {
            PlayAnimation(AnimationID, false, true);
        }
        public void PlayAnimation(object AnimationID, bool LoopAnimation)
        {
            PlayAnimation(AnimationID, LoopAnimation, true);
        }
        public void PlayAnimation(object AnimationID, bool LoopAnimation, bool StopOtherAnimations)
        {
            if (StopOtherAnimations)
            {
                _playinganimationdurationlist.Clear();
                _playinganimationlooplist.Clear();
            }
            if (!_animationlist.ContainsKey(AnimationID)) return;
            if (_playinganimationdurationlist.ContainsKey(AnimationID))
            {
                _playinganimationdurationlist[AnimationID] = Time.Zero;
                _playinganimationlooplist[AnimationID] = LoopAnimation;
            }
            else
            {
                _playinganimationdurationlist.Add(AnimationID, Time.Zero);
                _playinganimationlooplist.Add(AnimationID, LoopAnimation);
            }
        }
        public void StopAnimation(object AnimationID)
        {
            if (!_playinganimationdurationlist.ContainsKey(AnimationID)) return;
            _playinganimationdurationlist.Remove(AnimationID);
            _playinganimationlooplist.Remove(AnimationID);
        }
        public void StopAllAnimations()
        {
            foreach (object id in AnimationsPlaying)
            {
                StopAnimation(id);
            }
        }
        public void Update(Time DeltaTime)
        {
            List<object> eraselist = null;
            foreach (object id in AnimationsPlaying)
            {
                _playinganimationdurationlist[id] += DeltaTime;
                if (_playinganimationdurationlist[id] > _animationdurationlist[id])
                {
                    if (_playinganimationlooplist[id])
                    {
                        while (_playinganimationdurationlist[id] > _animationdurationlist[id])
                        {
                            _playinganimationdurationlist[id] -= _animationdurationlist[id];
                        }
                    }
                    else
                    {
                        if (eraselist == null) eraselist = new List<object>();
                        eraselist.Add(id);
                    }
                }
            }
            if (eraselist != null)
            {
                foreach (object id in eraselist)
                {
                    StopAnimation(id);
                }
            }
        }
        public void Animate(AnimatedObject<T> AnimatedObject)
        {
            foreach (object id in _playinganimationdurationlist.Keys)
            {
                _animationlist[id].Animate(AnimatedObject, (float)(_playinganimationdurationlist[id].Seconds / _animationdurationlist[id].Seconds));
            }
        }
        #endregion
    }
}