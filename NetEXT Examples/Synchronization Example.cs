﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Net;
using NetEXT.Networking;
using NetEXT.MathFunctions;
using NetEXT.Concurrent;

namespace NetEXT.Examples
{
    public class Synchronization_Example
    {
        #region Consts
        private const long CoordinateTypeID = 6;
        #endregion

        #region Coordinate
        private class Coordinate : SyncedObject
        {
            public override long TypeID { get { return CoordinateTypeID; } }
            public SyncedInt X = null;
            public SyncedInt Y = null;
            public Coordinate()
            {
                // Initialization order *must* remain the same
                X = new SyncedInt(200, this, SynchronizationType.Dynamic);
                Y = new SyncedInt(300, this, SynchronizationType.Dynamic);
            }
        }
        #endregion

        #region Variables
        private Thread _exitthread = null;
        private bool _shouldexit = false;
        #endregion

        #region Functions
        public void RunServer()
        {
            RunExitThread();
            TCPListener listener = new TCPListener(31355);
            List<Coordinate> coordinates = new List<Coordinate>();
            SynchronizerServer synchronizer = new SynchronizerServer();
            listener.ClientConnected += (server, client) => { synchronizer.AddClient(client); };
            listener.Start();
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            coordinates.Add(synchronizer.CreateObject<Coordinate>());
            foreach (var coord in coordinates)
            {
                coord.X.Value += RandomGenerator.Random(-100, 100);
                coord.Y.Value += RandomGenerator.Random(-100, 100);
            }
            while (!_shouldexit)
            {
                Dispatcher.CurrentDispatcher.HandleCallbacks(); // required so our async callbacks happen on the main thread
                synchronizer.Update();
                Thread.Sleep(16);
            }
            foreach (var link in synchronizer.ActiveLinks)
            {
                link.Shutdown();
            }
        }
        public void RunClient()
        {
            RunExitThread();
            TCPSocket client = new TCPSocket(31355, IPAddress.Loopback);
            SynchronizerClient synchronizer = new SynchronizerClient();
            List<Coordinate> coordinates = new List<Coordinate>();
            synchronizer.SetLifetimeManager(CoordinateTypeID, () =>
                {
                    Coordinate coord = new Coordinate();
                    coordinates.Add(coord);
                    return coord;
                }, (coordinate) =>
                {
                    coordinates.Remove((Coordinate)coordinate);
                    // Dispose() call is required for proper removal of objects from the synchronizer
                    coordinate.Dipose();
                });
            bool connected = false;
            client.Connected += (serverlink) => { synchronizer.AddServer(serverlink); connected = true; };
            client.Connect();
            while (!_shouldexit)
            {
                Dispatcher.CurrentDispatcher.HandleCallbacks(); // required so our async callbacks happen on the main thread
                if (!client.IsConnected && connected)
                {
                    client.Shutdown();
                    break;
                }
                synchronizer.Update();
                Thread.Sleep(16);
            }
            if (client.IsConnected) client.Disconnect();
        }
        private void RunExitThread()
        {
            Console.WriteLine("Press ENTER to exit...");
            _exitthread = new Thread(new ThreadStart(() =>
            {
                Console.ReadLine();
                _shouldexit = true;
            }));
            _exitthread.Start();
        }
        #endregion
    }
}
