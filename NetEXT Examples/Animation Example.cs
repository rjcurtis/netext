﻿using System;
using SFML.Window;
using SFML.Graphics;
using NetEXT.TimeFunctions;
using NetEXT.Animation;

namespace NetEXT.Examples
{
    public class Animation_Example
    {
        #region Variables
        private Animator<Sprite> _animator = new Animator<Sprite>();
        #endregion

        #region Functions
        public void Run()
        {
            RenderWindow window = new RenderWindow(new VideoMode(300, 200), "NetEXT Animation Example");
            window.SetVerticalSyncEnabled(false);
            window.SetFramerateLimit(60);
            window.KeyPressed += KeyPressed;
            Font font = new Font(".\\sansation.ttf");
            Text instructions = new Text("W: Play walk animation (loop)\nA: Play attack animation\nS: Stop current animation", font, 12);
            Text animationtext = new Text("", font, 12);
            animationtext.Position = new Vector2f(100, 150);
            Image image = new Image(".\\animation.png");
            image.CreateMaskFromColor(Color.White);
            Texture texture = new Texture(image);
            Sprite sprite = new Sprite(texture);
            sprite.Position = new Vector2f(100, 100);
            var walk = new FrameAnimation<Sprite>();
            AddFrames(walk, 0, 0, 7);
            AddFrames(walk, 0, 6, 0);
            var attack = new FrameAnimation<Sprite>();
            AddFrames(attack, 1, 0, 3);
            AddFrames(attack, 1, 4, 4, 5);
            for (int i = 0; i < 3; i++) { AddFrames(attack, 1, 5, 7); }
            AddFrames(attack, 1, 4, 4, 5);
            AddFrames(attack, 1, 3, 0);
            var stand = new FrameAnimation<Sprite>();
            AddFrames(stand, 0, 0, 0);
            _animator.AddAnimation("walk", walk, Time.FromSeconds(1));
            _animator.AddAnimation("stand", stand, Time.FromSeconds(1));
            _animator.AddAnimation("attack", attack, Time.FromSeconds(1));
            Clock frameclock = new Clock();
            while (window.IsOpen())
            {
                window.DispatchEvents();
                if (_animator.AnimationsPlaying.Length == 0) _animator.PlayAnimation("stand");
                if (_animator.AnimationsPlaying.Length > 0)
                {
                    animationtext.DisplayedString = "Animation: " + _animator.AnimationsPlaying[0];
                }
                else
                {
                    animationtext.DisplayedString = "";
                }
                _animator.Update(frameclock.Restart());
                _animator.Animate(sprite);
                window.Clear(new Color(50, 50, 50));
                window.Draw(instructions);
                window.Draw(animationtext);
                window.Draw(sprite);
                window.Display();
            }
        }
        private void KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.W)
            {
                _animator.PlayAnimation("walk", true);
            }
            else if (e.Code == Keyboard.Key.A)
            {
                _animator.PlayAnimation("attack");
            }
            else if (e.Code == Keyboard.Key.S)
            {
                _animator.StopAllAnimations();
            }
        }
        private void AddFrames(FrameAnimation<Sprite> Animation, int X, int YFirst, int YLast)
        {
            AddFrames(Animation, X, YFirst, YLast, 1);
        }
        private void AddFrames(FrameAnimation<Sprite> Animation, int X, int YFirst, int YLast, float Duration)
        {
            int step = (YFirst < YLast) ? +1 : -1;
            YLast += step;
            for (int y = YFirst; y != YLast; y += step)
            {
                Animation.AddFrame(Duration, new IntRect(36 * X, 39 * y, 36, 39));
            }
        }
        #endregion
    }
}
