﻿using SFML.Window;
using SFML.Graphics;
using NetEXT.Input;

namespace NetEXT.Examples
{
    public class Action_Example
    {
        #region Enums
        private enum MyAction
        {
            Run,
            Shoot,
            Quit,
            Resize
        }
        #endregion

        #region Functions
        public void Run()
        {
            RenderWindow window = new RenderWindow(new VideoMode(400, 300), "NetEXT Action Example");
            window.SetVerticalSyncEnabled(false);
            window.SetFramerateLimit(60);
            ActionMap map = new ActionMap();
            map[MyAction.Run] = (new Action(Keyboard.Key.LShift) | new Action(Keyboard.Key.RShift)) & new Action(Keyboard.Key.R);
            map[MyAction.Shoot] = new Action(Mouse.Button.Left, ActionType.PressOnce) | new Action(new Joy(0).Button(2), ActionType.PressOnce);
            map[MyAction.Quit] = new Action(Keyboard.Key.Escape, ActionType.ReleaseOnce) | new Action(EventType.Closed);
            map[MyAction.Resize] = new Action(EventType.Resized);
            EventSystem<ActionContext> system = ActionMap.CreateCallbackSystem();
            system.Connect(MyAction.Resize, OnResize);
            system.Connect(MyAction.Shoot, OnShoot);
            while (window.IsOpen())
            {
                map.Update(window);
                if (map.IsActive(MyAction.Run)) System.Console.WriteLine("Run!");
                if (map.IsActive(MyAction.Quit)) window.Close();
                map.InvokeCallbacks(system, window);
                window.Clear();
                window.Display();
            }
        }
        public void OnResize(ActionContext Context)
        {
            System.Console.WriteLine("Resized!   New size = (" + Context.Event.Value.Size.Width + ", " + Context.Event.Value.Size.Height + ")");
        }
        public void OnShoot(ActionContext Context)
        {
            System.Console.WriteLine("Shoot: (" + Mouse.GetPosition(Context.Window).X + ", " + Mouse.GetPosition(Context.Window).Y + ")");
        }
        #endregion
    }
}
