NetEXT -- This software is provided 'as-is' without any express or
implied warranty. Read the full license agreement in "license.txt".

NetEXT is a boilerplate library that provides extensions to SFML
(http://www.sfml-dev.org) for all CLR languages. It is largely a
port of Thor (http://www.bromeon.ch/libraries/thor/) that brings
the functionality to all CLR languages.


Current Features

*Large Texture/Sprite/RenderTexture classes
*Particle System complete with the ability to build custom Emitters/Affectors
*Animation Module that supports custom Animation classes
*Vector Math Module
*Time module providing SFML Time and Clock class and different timer wrappers
   around the Clock class
*Complete Input Module with customizable actions and callbacks
*Networking module [WIP] [BETA]


Documentation

There is no documentation available for just NetEXT, instead you should reference
Thor's documentation (http://www.bromeon.ch/libraries/thor/v2.0/doc/index.html).